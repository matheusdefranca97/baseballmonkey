using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableTrigger : MonoBehaviour
{

    bool canDisableTrigger = true;
    CapsuleCollider capsuleCollider;
    private void Awake()
    {
        capsuleCollider = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canDisableTrigger)
            StartCoroutine("DisableTriggerCoroutine");
    }

    IEnumerator DisableTriggerCoroutine()
    {


        yield return new WaitForSeconds(0.5f);
        canDisableTrigger = false;
        capsuleCollider.enabled = false;
        yield return new WaitForSeconds(0.7f);
        capsuleCollider.enabled = true;
        canDisableTrigger = true;
    }
}
