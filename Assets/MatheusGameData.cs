﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MatheusGameData : MonoBehaviour
{

    public List<int> charactersPositionUnlocked;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GoToScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }


}
