using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum ItemType
{
    character, bat
}

public class ShopItem : MonoBehaviour
{

    [SerializeField] ItemType itemType;
    [SerializeField] int id;
    [SerializeField] int price;
    [SerializeField] string name;
    [SerializeField] Sprite itemSprite;


    bool purchased;

    [Header("UI")]

    [SerializeField] Image itemImage;
    [SerializeField] TextMeshProUGUI priceText, nameText;
    [SerializeField] Button selectButton, buyButton;


    private void Start()
    {
        selectButton.onClick.AddListener(SelectButtonClick);
        buyButton.onClick.AddListener(BuyButtonClick);
        PlayerPrefs.SetInt("purchased" + itemType.ToString() + 0, 1);
        PlayerPrefs.SetInt("purchased" + itemType.ToString() + 1, 1);
        SetupItem();
    }
    public void SetupItem()
    {
        purchased = (PlayerPrefs.GetInt("purchased" + itemType.ToString() + id) == 0) ? false : true;
        nameText.text = name;
        priceText.text = price.ToString();
        buyButton.gameObject.SetActive(!purchased);
        selectButton.gameObject.SetActive(purchased);
        itemImage.sprite = itemSprite;
    }

    public void OnItemBuyed()
    {
        purchased = true;
        PlayerPrefs.SetInt("purchased" + itemType.ToString() + id, 1);
        SetupItem();
    }

    public async void BuyButtonClick()
    {
        // if (await ThirdwebShop.instance.BurnToken(price))
        // {

        //     Debug.Log("funfou");
        //     OnItemBuyed();
        // }
        // else
        // {
        //     Debug.Log("não funfou");
        // }
    }

    public void SelectButtonClick()
    {
        //SetupItem();

        if (itemType == ItemType.character)
        {
            FindObjectOfType<PlayerFire>().SetupCharacter(id);
        }
        else if (itemType == ItemType.bat)
        {
            BatSelection.instance.SetupCurrentBat(id);
        }

        Debug.Log("entrei");
    }

}
