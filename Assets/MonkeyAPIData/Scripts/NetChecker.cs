﻿using UnityEngine;
using System.Collections;

public class NetChecker  : MonoBehaviour{
	private static bool isNetOn = false;
	public static bool IsNetAvail{
		get{
			return isNetOn;
		}
		set{
			isNetOn = value;
			if (isNetOn) {
				
//				if(!MyFbManager.instance.isFBInitialized)
//					MyFbManager.instance.initFBManager();
			} 				
//			UiManager.ins.showHideNetMsgPanel(!isNetOn);
		}
	}

	void Start(){
		StartCoroutine(checkInternet());		
	}

	IEnumerator checkInternet(){
		WWW www = new WWW ("https://www.google.com"); 

		yield return www;

		if (www.isDone) {
			if (www.error != null)
				IsNetAvail = false;
			else
				IsNetAvail = true;
		}

		if (!IsNetAvail) {
			yield return new WaitForSeconds (2.0f);
			StartCoroutine(checkInternet());
		} else {
			yield return new WaitForSeconds (10.0f);
			StartCoroutine(checkInternet());
		}
		
	}
}
