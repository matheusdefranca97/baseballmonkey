﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class AllCoins{
	public ulong MonkeyCoins = 100;//1000000;
	public ulong RaffleCoins = 0;
	public ulong BountyCoins = 0;
	public ulong DabalooneyCoins = 0;
	public ulong ScavengerCoins = 0;
}

public class CoinsManager : MonoBehaviour {

	public static CoinsManager ins;

	public const string Key_Preferences_Set 		= "Key_Preferences_Set";
	public const string Key_User_Coins 				= "Key_User_Coins";


	public const string Key_Play_One_Day 			= "Key_Play_One_Day";
	public const string Key_Play_One_Week 			= "Key_Play_One_Week";
	public const string Key_Play_One_Month 			= "Key_Play_One_Month";
	public const string Key_Play_One_Year 			= "Key_Play_One_Year";
	public const string Key_Play_Unlimited 			= "Key_Play_Unlimited";
	public const string Key_Remove_Add 				= "Key_Remove_Add";


	public ulong currCoins = 0;
	public int currRaffles = 0;

	[Header("Menu")]
	[SerializeField] Text totalCoinsTxt;
	[SerializeField] Text totalRafflesTxt;
	[SerializeField] Text totalBountyTxt;
	[SerializeField] Text totalDabalooneyTxt;

	void Awake(){
		ins = this;
	}

    private void Start() {
		//PlayerPrefs.DeleteKey("Key_Daily_Played_Rounds");
		//Debug.Log("GameStart :"+AllCoins.DabalooneyCoins);
        displayCoinsOnMenu();
    }

	void OnEnable ()
	{
		displayCoinsOnMenu();
	}

    public void displayCoinsOnMenu(){
		AllCoins aCoins = getUserCoinDetails();
//		Debug.Log("aCoins.DabalooneyCoins :"+aCoins.DabalooneyCoins);
		totalCoinsTxt.text = aCoins.MonkeyCoins.ToString();//""+PlayerPrefs.GetInt(CoinsManager.Key_User_Coins);
		totalRafflesTxt.text = aCoins.RaffleCoins.ToString();//""+PlayerPrefs.GetInt(CoinsManager.Key_User_Raffle_Coins);
		totalBountyTxt.text = aCoins.BountyCoins.ToString();
		totalDabalooneyTxt.text = aCoins.DabalooneyCoins.ToString();
	}

	public void saveUserCoins(AllCoins aCoins){
		PlayerPrefs.SetString (CoinsManager.Key_User_Coins,JsonUtility.ToJson(aCoins));
		PlayerPrefs.Save ();
		DataInserter.Din.UpdateUserCoins(Player.getPlayer().pId,aCoins,null);
	}

	public AllCoins getUserCoinDetails(){
		string details = PlayerPrefs.GetString (CoinsManager.Key_User_Coins, "");
		if(string.IsNullOrEmpty(details)){
			PlayerPrefs.SetString (Key_User_Coins, JsonUtility.ToJson(new AllCoins()));
			PlayerPrefs.Save ();	
			details = PlayerPrefs.GetString (CoinsManager.Key_User_Coins);
		}
			
		return JsonUtility.FromJson<AllCoins> (details);
	}
}
