﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player
{
	public string pId = "";
	public string pFbId = "";
	public string pName = "";
	public string pEmail = "";
	public string pPwd = "";
	public string pBorkerKey = "";
	public string pScore = "";

	//Method to get player from Preference
	public static Player getPlayer()
	{
		string playerDetails = PlayerPrefs.GetString(Common.KEY_PLAYER_DETAILS);
		playerDetails.Trim();
		if (!string.IsNullOrEmpty(playerDetails))
		{
			return JsonUtility.FromJson<Player>(playerDetails);
		}
		return null;
	}

	//Method to save player in Preference
	public static void savePlayer(string plyrDataString)
	{
		PlayerPrefs.SetString(Common.KEY_PLAYER_DETAILS, plyrDataString/*JsonUtility.ToJson(plyrDataString)*/);
		PlayerPrefs.Save();
	}
}


public class ServerUiManager : MonoBehaviour
{

	public static ServerUiManager ins;

	[SerializeField]
	InputField regNameText, regEmailText, regPwdText, regBrokerText;
	[SerializeField]
	Text regErrorTxt;
	[SerializeField]
	InputField loginEmailText, loginPwdText;
	[SerializeField]
	Text loginErrTxt;
	[SerializeField]
	InputField fbBrokerIF;

	void Awake()
	{
		ins = this;
	}

	void Start()
	{
		getUserScores();
	}

	void clearRegisterFields()
	{
		regNameText.text = regEmailText.text = "";
		regPwdText.text = regBrokerText.text = "";
		regErrorTxt.text = "";
	}

	void clearLoginFields()
	{
		loginEmailText.text = loginPwdText.text = "";
		loginErrTxt.text = "";
	}

	public void createNewAccount()
	{
		clearRegisterFields();

		Debug.LogError("Show Register Popup");
		ScenePanelMgr.Ins.showPanel(PanelType.RegisterPanel);
		//PopUpsManager.ins.openScreen (MyScreens.Register);
	}

	public void backToLogin()
	{
		clearLoginFields();
		Debug.LogError("Show Login Popup");
		//ScenePanelMgr.Ins.showPanel(PanelType.LoginPanel);
		//PopUpsManager.ins.openScreen (MykScreens.LogIn);	
	}

	Player regPlayer;
	public void OnRegisterClicked()
	{
		if (NetChecker.IsNetAvail)
		{

			regPlayer = new Player();
			//get data from ui
			regPlayer.pName = regNameText.text;
			regPlayer.pEmail = regEmailText.text;
			regPlayer.pPwd = regPwdText.text;
			regPlayer.pBorkerKey = regBrokerText.text;
			//check if all fields are valid and contain data
			if (string.IsNullOrEmpty(regPlayer.pName))
				StartCoroutine(showError(regErrorTxt, "Please enter your name"));
			else if (string.IsNullOrEmpty(regPlayer.pEmail))
				StartCoroutine(showError(regErrorTxt, "Please enter your email"));
			else if (string.IsNullOrEmpty(regPlayer.pPwd))
				StartCoroutine(showError(regErrorTxt, "Please enter your password"));
			else if (!Common.IsValidEmailAddress(regPlayer.pEmail))
				StartCoroutine(showError(regErrorTxt, "Invalid email id"));
			else
			{
				print("regPlayer.pFbId " + regPlayer.pFbId);

				Debug.LogError("Show Loading Panel here");
				ScenePanelMgr.Ins.showHideLoading(true);
				//PopUpsManager.ins.showHideLoading (true);
				//register player
				DataInserter.Din.RegisterUSer(regPlayer.pName, regPlayer.pPwd, regPlayer.pEmail, regPlayer.pFbId, regPlayer.pBorkerKey, OnRegisterCallback);
			}
		}
		else
		{
			Debug.LogError("Open Internet panel here");
			ScenePanelMgr.Ins.openInternetPanel();
			//PopUpsManager.ins.openInternetPanel ();
		}
	}

	//{"msg":"Registration success","id":82} 
	//{"msg":"User already exists","id":"82"}
	void OnRegisterCallback(WWW www)
	{
		Debug.LogError("Hide Loading Panel here");
		ScenePanelMgr.Ins.showHideLoading(false);
		//PopUpsManager.ins.showHideLoading (false);
		print("Register " + www.text + " " + www.error);
		if (string.IsNullOrEmpty(www.error))
		{
			JSONObject response = new JSONObject(www.text);
			if (response != null)
			{
				Debug.Log("Register Callback: " + response.ToString());
				string msg = response.GetField("msg").ToString().Replace("\"", "");
				if (msg.Contains("Registration success"))
				{
					string userId = response.GetField("id").ToString().Replace("\"", "");
					//Debug.Log(regPlayer.pId+": userId :"+userId);
					regPlayer.pId = userId;
					//save player details
					Player.savePlayer(JsonUtility.ToJson(regPlayer));
					//PlayerPrefs.SetString(MyPrefs.KEY_PLAYER_DETAILS,);
					//PlayerPrefs.Save ();
					Debug.LogError("Open Menu Screen here");
					ScenePanelMgr.Ins.showPanel(PanelType.Home_Panel);
					//PopUpsManager.ins.openScreen (MyScreens.Menu);
					ServerUiManager.ins.getUserScores();
					return;
				}
				else if (msg.Contains("User already exists"))
				{
					StartCoroutine(showError(regErrorTxt, "This email is already registered,Please choose another."));
					return;
				}
			}
		}

		StartCoroutine(showError(regErrorTxt, "Error registering,Please try again later "));

	}

	IEnumerator showError(Text errTxt, string msg)
	{
		errTxt.text = msg;
		yield return new WaitForSeconds(2.0f);
		errTxt.text = "";
	}

	public void OnLoginClicked()
	{
		if (NetChecker.IsNetAvail)
		{
			regPlayer = new Player();
			//get data from ui
			regPlayer.pEmail = loginEmailText.text;
			regPlayer.pPwd = loginPwdText.text;
			//check if all fields are valid and contain data
			if (string.IsNullOrEmpty(regPlayer.pEmail))
				StartCoroutine(showError(loginErrTxt, "Please enter your email"));
			else if (string.IsNullOrEmpty(regPlayer.pPwd))
				StartCoroutine(showError(loginErrTxt, "Please enter your password"));
			else if (!Common.IsValidEmailAddress(regPlayer.pEmail))
				StartCoroutine(showError(loginErrTxt, "Invalid email id"));
			else
			{
				Debug.LogError("Show Loading Panel here");
				ScenePanelMgr.Ins.showHideLoading(true);
				//PopUpsManager.ins.showHideLoading (true);
				//register player
				DataInserter.Din.LoginUser(regPlayer.pEmail, regPlayer.pPwd, OnLoginCallback);
			}

		}
		else
		{
			Debug.LogError("Open Internet Panel here");
			ScenePanelMgr.Ins.openInternetPanel();
			//PopUpsManager.ins.openInternetPanel ();
		}

	}
	void OnLoginCallback(WWW www)
	{

		Debug.LogError("Hide Loading Panel here");
		ScenePanelMgr.Ins.showHideLoading(false);
		//PopUpsManager.ins.showHideLoading (false);

		if (string.IsNullOrEmpty(www.error))
		{

			print(" login : " + www.text);

			JSONObject response = new JSONObject(www.text);
			if (response != null)
			{
				string msg = response.GetField("msg").ToString().Replace("\"", "");
				if (msg.Contains("User login Successfully"))
				{
					string userId = response.GetField("id").ToString().Replace("\"", "");
					regPlayer.pId = userId;
					//save player details
					Player.savePlayer(JsonUtility.ToJson(regPlayer));
					//PlayerPrefs.SetString (MyPrefs.KEY_PLAYER_DETAILS, JsonUtility.ToJson (regPlayer));
					//PlayerPrefs.Save ();
					Debug.LogError("Show MySceen Menu here");
					ScenePanelMgr.Ins.showPanel(PanelType.Home_Panel);
					//PopUpsManager.ins.openScreen (MyScreens.Menu);
					ServerUiManager.ins.getUserScores();
					return;
				}
				else if (msg.Contains("User already exists"))
				{
					StartCoroutine(showError(loginErrTxt, "This email is already registered,Please choose another."));
					return;
				}
				else if (msg.Contains("Wrong Password"))
				{
					StartCoroutine(showError(loginErrTxt, "Password mismatch"));
					return;
				}
			}
		}
		StartCoroutine(showError(loginErrTxt, "Error login,Please try again later "));

	}



	#region Coin Conversion

	public void OnExchangeClicked()
	{

		//check if user is connected to net and is registered
		if (NetChecker.IsNetAvail)
		{
			if (!string.IsNullOrEmpty(Player.getPlayer().pId))
			{
				//get current conversion values
				Debug.LogError("Open Coin convert Panel here");
				//PopUpsManager.ins.openScreen (MyScreens.CoinConvert);
				//set initial count rate to one


			}
			else
			{
				Debug.LogError("Open ExchangeNotLogin Panel here");
				//PopUpsManager.ins.openScreen (MyScreens.ExchangeNotLogin);				
			}
		}
		else
		{
			Debug.LogError("Open Internet Panel here");
			ScenePanelMgr.Ins.openInternetPanel();
			//PopUpsManager.ins.openInternektPanel ();
		}

	}
	public void notLoginYesClicked()
	{
		Debug.LogError("Open Login Panel here");
		//ScenePanelMgr.Ins.showPanel(PanelType.LoginPanel);
		//PopUpsManager.ins.openScreen (MyScreens.LogIn);
	}

	public void notLoginNoClicked()
	{
		Debug.LogError("Open Menu Panel here");
		ScenePanelMgr.Ins.showPanel(PanelType.Home_Panel);
		//PopUpsManager.ins.openScreen (MyScreens.Menu);
	}

	public void getUserScores()
	{

		Player pl = Player.getPlayer(); //JsonUtility.FromJson<Player>(PlayerPrefs.GetString (MyPrefs.KEY_PLAYER_DETAILS));
		if (!string.IsNullOrEmpty(pl.pId))
		{
			//register player
			DataInserter.Din.GetUserCoinsInfo(pl.pId, OnUserScoresGotCallback);
		}
	}

	//CoinsManager _CoinManager;
	// [{"Coins Name":"BountyCoins","Coins Value":"0"},{"Coins Name":"MonkeyCoins","Coins Value":"0"},{"Coins Name":"DablooneyCoins","Coins Value":"0"},{"Coins Name":"RaffleCoins","Coins Value":"0"}]
	void OnUserScoresGotCallback(WWW www)
	{

		if (string.IsNullOrEmpty(www.error))
		{
			print(" Score : " + www.text);

			JSONObject response = new JSONObject(www.text);
			AllCoins gotCoins = new AllCoins();
			if (response != null)
			{
				for (int i = 0; i < response.Count; i++)
				{
					string cName = response[i].GetField("Coins Name").ToString().Replace("\"", "");
					ulong cVal = ulong.Parse(response[i].GetField("Coins Value").ToString().Replace("\"", ""));

					//					print (" "+cName+" "+cVal);
					if (cName == MonkeyCoinType.BountyCoins.ToString())
						gotCoins.BountyCoins = cVal;
					else if (cName == MonkeyCoinType.MonkeyCoins.ToString())
						gotCoins.MonkeyCoins = cVal;
					else if (cName == MonkeyCoinType.DablooneyCoins.ToString())
						gotCoins.DabalooneyCoins = cVal;
					else if (cName == MonkeyCoinType.RaffleCoins.ToString())
						gotCoins.RaffleCoins = cVal;
				}
				//CoinsManager _CoinManager = FindObjectOfType<CoinsManager>().displayCoinsOnMenu();
				CoinsManager.ins.displayCoinsOnMenu();
				//update to local
				CoinsManager.ins.saveUserCoins(gotCoins);

				CoinsManager.ins.displayCoinsOnMenu();
			}
		}
	}

	#endregion

}
