﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConvertCoinUi : MonoBehaviour {

	int currMDValue = 1;
	int currMRValue = 1;
	int currDRValue = 1;

	[SerializeField]
	Text mdTextCount,mrTxtCount,drTxtCount;

	// Use this for initialization
	void OnEnable () {

		AllCoins aCoins = CoinsManager.ins.getUserCoinDetails();
		currMDValue = currMRValue = currDRValue = 1;
		setCount ();
	}

	void setCount(){
		mdTextCount.text = currMDValue + "";
		mrTxtCount.text = currMRValue + "";
		drTxtCount.text = currDRValue + "";
	}

	// Update is called once per frame
	public void OnMdValueChanged () {
		if (currMDValue < 5)
			currMDValue++;
		else
			currMDValue = 1;
		mdTextCount.text = currMDValue + "";
	}
	public void OnMrValueChanged () {
		if (currMRValue < 5)
			currMRValue++;
		else
			currMRValue = 1;
		mrTxtCount.text = currMRValue + "";
	}
	public void OnDrValueChanged () {
		if (currDRValue < 5)
			currDRValue++;
		else
			currDRValue = 1;
		drTxtCount.text = currDRValue + "";
	}

	public void convertCoins_Mon_to_Dab(){
		print ("convert md called "+currMDValue);

        Debug.LogError("Show Loading Panel here");
        ScenePanelMgr.Ins.showHideLoading(true);
		//PopUpsManager.ins.showHideLoading (true);

        Player player = Player.getPlayer ();
		//call api to convert coins
//		DataInserter.Din.CoinConvertType(player.pFbId,player.pId,MonkeyCoinType.MonkeyCoins.ToString(),MonkeyCoinType.DablooneyCoins.ToString(),currMDValue,(wwwData)=>{
			DataInserter.Din.CoinConvertType(player.pFbId,player.pId,MonkeyCoinType.MonkeyCoins.ToString(),MonkeyCoinType.DablooneyCoins.ToString(),100000,(wwwData)=>{

            Debug.LogError("Hide Loading Panel here");
            ScenePanelMgr.Ins.showHideLoading(false);
            //PopUpsManager.ins.showHideLoading (false);

            if (string.IsNullOrEmpty(wwwData.error)){
			    print("Coin converted "+wwwData.text);

				InfoPopup.Ins.showSingleBtnInfoPopup("Need More Monkey Coins Converted Faild", "OK");
				return;

			}else{
				print("Error in data"+wwwData.error);
			}
		});
	}

	public void convertCoins_Mon_to_Raf(){
		print ("convert md called "+currMRValue);

        Debug.LogError("Show Loading Panel here");
        ScenePanelMgr.Ins.showHideLoading(true);
        //PopUpsManager.ins.showHideLoading (true);

        Player player = Player.getPlayer ();
		//call api to convert coins
		DataInserter.Din.CoinConvertType(player.pFbId,player.pId,MonkeyCoinType.MonkeyCoins.ToString(),MonkeyCoinType.RaffleCoins.ToString(),currMRValue,(wwwData)=>{

            Debug.LogError("Hide Loading Panel here");
            ScenePanelMgr.Ins.showHideLoading(false);
            //PopUpsManager.ins.showHideLoading (false);

            if (string.IsNullOrEmpty(wwwData.error)){
				print("Coin converted "+wwwData.text);
				InfoPopup.Ins.showSingleBtnInfoPopup("Need More Monkey Coins Converted Faild", "OK");
				return;
			}else{
				print("Error in data"+wwwData.error);

			}
		});
	}

	public void convertCoins_Dab_to_Raf(){
		print ("convert md called "+currDRValue);

        Debug.LogError("Show Loading Panel here");
        ScenePanelMgr.Ins.showHideLoading(true);
        //PopUpsManager.ins.showHideLoading (true);

        Player player = Player.getPlayer ();
		//call api to convert coins
		DataInserter.Din.CoinConvertType(player.pFbId,player.pId,MonkeyCoinType.DablooneyCoins.ToString(),MonkeyCoinType.RaffleCoins.ToString(),currDRValue,(wwwData)=>{

            Debug.LogError("Hide Loading Panel here");
            ScenePanelMgr.Ins.showHideLoading(false);
            //PopUpsManager.ins.showHideLoading (false);

            if (string.IsNullOrEmpty(wwwData.error)){
				print("Coin converted "+wwwData.text);
				InfoPopup.Ins.showSingleBtnInfoPopup("Need More Dablooney Coins points Converted Faild", "OK");
				return;
			}else{
				print("Error in data"+wwwData.error);
			}
		});
	}
}
