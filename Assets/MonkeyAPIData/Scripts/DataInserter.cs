﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MonkeyCoinType{
	MonkeyCoins, 
	DablooneyCoins, 
	BountyCoins, 
	RaffleCoins
}

public class DataInserter : MonoBehaviour {

	public const string domainName = "http://cubicalhub.com/monkey/";

	public const string RegisterUserURL 			= domainName + "register";//username,password,email,fb_id,user_key
	public const string LoginUserUrl				= domainName + "user-login";//email,password
	public const string UpdateUserProfileURL		= domainName + "update-fb-id";//fb_id,nuser_id
	public const string SetMultiCoinsURL 			= domainName + "set-coin";

//	public const string SendOppResultURL 			= domainName + "set-opponent-results";
//	public const string FetchPlayerScoreURL 		= domainName + "fetch-player-score"; 
	public const string CoinConvertTypeURL 			= domainName + "convert-coin-type";
	//get user score save at server of all coins
	public const string GetUserCoinsInfoURL 		= domainName + "get-coin";//user_id

	//update all user coins to server
	public const string UpdateUserCoinsURL			= domainName + "coins-update";//user_id,MonkeyCoins,DablooneyCoins,RaffleCoins,BountyCoins
	//get all contest
	public const string GetContestsURL				= domainName + "get-contest";//broker_key,user_id	get-contest/";
	//register for a contest
	public const string RegisterContestURL			= domainName +"contest-register";//contest_id,user_id


    public static DataInserter Din = null;
	void Awake(){
		if(Din != null){
			Destroy (this);
			return; 
			}
		Din = this;
		DontDestroyOnLoad (this);
	}

	void Start(){

	}

	public void GetUserCoinsInfo(string userId,System.Action<WWW> callbackActionGetMultiCoin)//Amit 29/11/17
	{
		WWWForm coinInfo = new WWWForm ();
		coinInfo.AddField ("user_id", userId);
		StartCoroutine (CO_webRequest(GetUserCoinsInfoURL,coinInfo,callbackActionGetMultiCoin));
	}

	public void UpdateUserCoins(string userId,AllCoins aCoins,System.Action<WWW> callback)//Amit 29/11/17
	{
		WWWForm coinInfo = new WWWForm ();
		coinInfo.AddField ("user_id", userId);
		coinInfo.AddField ("MonkeyCoins", aCoins.MonkeyCoins.ToString());
		coinInfo.AddField ("DablooneyCoins", aCoins.DabalooneyCoins.ToString());
		coinInfo.AddField ("RaffleCoins", aCoins.RaffleCoins.ToString());
		coinInfo.AddField ("BountyCoins", aCoins.BountyCoins.ToString());
		StartCoroutine (CO_webRequest(UpdateUserCoinsURL,coinInfo,callback));
	}

	public void RegisterContest(string userId, string contest_id,System.Action<WWW> callbackRegistr)
	{
		WWWForm form1 = new WWWForm ();

		form1.AddField ("user_id", userId);
		form1.AddField ("contest_id", contest_id);
		StartCoroutine (CO_webRequest(RegisterContestURL,form1,callbackRegistr));
	}

	public void GetAllContests(string userId, string broker_key,System.Action<WWW> callbackContest)
	{
		WWWForm form1 = new WWWForm ();

		form1.AddField ("user_id", userId);
		form1.AddField ("broker_key", broker_key);

		StartCoroutine (CO_webRequest(GetContestsURL,form1,callbackContest));
	}

	//username,password,email,fb_id,user_key
	public void RegisterUSer(string username,string password, string email, string fb_id, string Broker_key, System.Action<WWW> callbackAction)//Use in home Panel
	{
		WWWForm form = new WWWForm ();	
		
		form.AddField ("username", username);
		form.AddField ("password", password);
		form.AddField ("email", email/*+Random.Range(1,100)*/);
		form.AddField ("fb_id", fb_id);
		form.AddField ("user_key", Broker_key);
		
		StartCoroutine(CO_webRequest(RegisterUserURL,form,callbackAction));
	}

	public void LoginUser(string email, string password, System.Action<WWW> callbackAction)//Use in home Panel
	{
		WWWForm form = new WWWForm ();	

		form.AddField ("email", email);
		form.AddField ("password", password);

		StartCoroutine(CO_webRequest(LoginUserUrl,form,callbackAction));
	}
	//user_id,username,fb_id,email
	public void UpdateUserProfile(string userId,string uName, string FbId,string uEmail, System.Action<WWW> callbackAction)//Use in home Panel
	{
		WWWForm form = new WWWForm ();	

		form.AddField ("user_id", userId);
		form.AddField ("username", uName);
		form.AddField ("fb_id", FbId);
		form.AddField ("email", uEmail);

		StartCoroutine(CO_webRequest(UpdateUserProfileURL,form,callbackAction));
	}

	//fb_id,user_id,from_coin_type,to_coin_type,from_coin_count
	public void CoinConvertType(string fb_id,string userId,string fromCoinType,string toCoinType,int From_CoinCount, System.Action<WWW> callbackActionCoinConversion)
	{

		print (" "+fb_id+" "+userId+" "+fromCoinType+" "+toCoinType+" "+From_CoinCount);
		WWWForm coinConversion = new WWWForm ();
		coinConversion.AddField ("fb_id", fb_id);
		coinConversion.AddField ("user_id", userId);
		coinConversion.AddField ("from_coin_type", fromCoinType);
		coinConversion.AddField ("to_coin_type", toCoinType);
		coinConversion.AddField ("from_coin_count", From_CoinCount);

		StartCoroutine (CO_webRequest(CoinConvertTypeURL, coinConversion, callbackActionCoinConversion));
	}
	//game_id,device_id,coin_name,coin_points,user_id
	public void SetMultiCoin(int gameId ,string deviceId ,string coinName ,ulong coinPoints ,string userId ,System.Action<WWW> callbackActionSetMultiCoin)
	{
		WWWForm setMultiCoinForm = new WWWForm ();

		setMultiCoinForm.AddField ("game_id", gameId);
		setMultiCoinForm.AddField ("device_id", deviceId);
		setMultiCoinForm.AddField ("coin_name", coinName);
		setMultiCoinForm.AddField ("coin_points", coinPoints.ToString());
		setMultiCoinForm.AddField ("user_id", int.Parse(userId));

		StartCoroutine (CO_webRequest(SetMultiCoinsURL,setMultiCoinForm,callbackActionSetMultiCoin));
	}


	IEnumerator CO_webRequest(string url, WWWForm form, System.Action<WWW> callbackAction)
	{
		WWW www;

		if (form != null) {
			www = new WWW (url, form);
		} else {
			www = new WWW (url);
		}
		yield return www;

		if(callbackAction != null)
		callbackAction.Invoke(www);
	}


	
}
