using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusCoin : MonoBehaviour
{
    ParticleSystemWork _particleSystem;
    public TextMesh textObject;
    void Awake()
    {
        _particleSystem = FindObjectOfType<ParticleSystemWork>();
    }
    void Start()
    {
        AppManager.EvtGameStart += gameStarted;
        AppManager.EvtGamePlayEnd += gameEnd;
    }

    void OnDestroy()
    {
        AppManager.EvtGameStart -= gameStarted;
        AppManager.EvtGamePlayEnd -= gameEnd;
    }

    void gameStarted()
    {
        Debug.Log("gameStarted");
    }

    void gameEnd()
    {
        Destroy(gameObject);
        Debug.Log("gameEnd");
    }

    private void OnTriggerEnter(Collider other)
    {
        AudioManager.ins.playEffectSound(AudioManager.ins.baloonHitClip);
        StartCoroutine("Co_WaitForBlast");
    }


    IEnumerator Co_WaitForBlast()
    {
        _particleSystem.startPArticle();
        yield return new WaitForSeconds(0.3f);
        FindObjectOfType<BonusCoinManager>().HitCoin();
        Destroy(gameObject);
    }

    public void FinishMovement()
    {
        FindObjectOfType<BonusCoinManager>().InstantiateCoin();
        Destroy(gameObject);
    }
}
