﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelectionManager : MonoBehaviour
{

    public List<GameObject> allCharacters;
    public List<AnimationClip> allAnimations;
    public Text nameText;

    public static int auxNumber = 0;

    public Transform prevPosition, nextPosition, middlePosition, gorillaMiddlePosition;
    public GameObject prevButton, nextButton;

    // Start is called before the first frame update
    void Start()
    {
        CheckButtons();
        allCharacters[auxNumber].transform.DOMove(middlePosition.position, 2f);
        allCharacters[auxNumber].GetComponent<Animation>().Play();
        nameText.text = allCharacters[auxNumber].name;
        auxNumber++;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(auxNumber);
    }

    public void ClickRightButton()
    {


        if (auxNumber > 0)
        {
            allCharacters[auxNumber - 1].transform.DOMove(nextPosition.position, 2f);
        }
        nameText.text = allCharacters[auxNumber].name;
        if (auxNumber == 7 || auxNumber == 15)
        {
            allCharacters[auxNumber].transform.DOMove(gorillaMiddlePosition.position, 2f);
        }
        else
        {
            allCharacters[auxNumber].transform.DOMove(middlePosition.position, 2f);
        }
        allCharacters[auxNumber].GetComponent<Animation>().Play();
        auxNumber++;
        CheckButtons();

    }

    public void ClickLeftButton()
    {

        if (auxNumber <= allCharacters.Count)
        {
            allCharacters[auxNumber - 1].transform.DOMove(prevPosition.position, 2f);
        }
        nameText.text = allCharacters[auxNumber - 2].name;
        if (auxNumber - 2 == 7 || auxNumber - 2 == 15)
        {
            allCharacters[auxNumber - 2].transform.DOMove(gorillaMiddlePosition.position, 2f);
        }
        else
        {
            allCharacters[auxNumber - 2].transform.DOMove(middlePosition.position, 2f);
        }
        allCharacters[auxNumber - 2].GetComponent<Animation>().Play();
        auxNumber--;
        CheckButtons();
    }

    public void CheckButtons()
    {
        if (auxNumber <= 1)
        {
            prevButton.SetActive(false);
        }
        else if (auxNumber >= allCharacters.Count)
        {
            nextButton.SetActive(false);
        }
        else
        {
            nextButton.SetActive(true);
            prevButton.SetActive(true);
        }
    }

    public void ChooseCharacter()
    {
        SceneManager.LoadSceneAsync("GamePlay");
    }
}
