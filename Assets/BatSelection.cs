using System.Collections;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using UnityEngine;

public class BatSelection : MonoBehaviour
{

    public static BatSelection instance;

    [SerializeField] public GameObject batPrefab;
    [SerializeField] List<GameObject> allBats;

    [SerializeField] List<Material> allMaterials;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        SetupCurrentBat(0);
    }

    public void SetupCurrentBat(int batID)
    {
        SetupMaterial(batID, FindCurrentBat());
    }

    public GameObject FindCurrentBat()
    {
        Debug.Log(allBats.Count);

        for (int i = 0; i < allBats.Count; i++)
        {
            if (allBats[i].activeInHierarchy)
            {
                return allBats[i];
            }
        }

        return null;
    }

    public void SetupMaterial(int materialID, GameObject currentBat)
    {
        currentBat.GetComponent<MeshRenderer>().material = allMaterials[materialID];
    }
}
