﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    public float fireStrength = 500;
    public List<Animator> allCharacters;


    //	public float adjustmentFactor = 0.02f;
    public Vector3 angularVelo = Vector3.zero;

    [SerializeField] Transform ballDirectorTrans;

    Animator pitcherAnimator;
    Transform ballHitTrans = null;
    //Temp method to swing bat
    public Animator batterAnimator;

    private void Awake()
    {
        SetupCharacter(0);
    }


    public void SetupCharacter(int characterID)
    {
        batterAnimator.gameObject.SetActive(false);
        batterAnimator = allCharacters[characterID];
        batterAnimator.gameObject.SetActive(true);

    }
    void Start()
    {
        pitcherAnimator = GetComponentInChildren<Animator>();
        ballHitTrans = GameObject.Find("BallHitCube").transform;
    }

    [SerializeField] GameObject pitcherSpecialBallEffect;
    public void showSpecialBallEffectOnPitcher()
    {
        // Debug.Log("Special Ball");
        //Run Specicial effect around the Pitcher character
        pitcherSpecialBallEffect.SetActive(true);

        //Also Stop Effect after someTime
        StartCoroutine(Common.CO_ProvideDelay(4f, () =>
        {
            pitcherSpecialBallEffect.SetActive(false);
        }));
    }

    public void runBallThrowAnimationOnPitcher()
    {
        pitcherAnimator.SetTrigger("Throw");
    }

    public void forcePitcherToIdle()
    {
        pitcherAnimator.SetTrigger("ForceIdle");

        if (throwBallSim != null)
        {
            throwBallSim.resetThrowSimulator();
        }

        //		StopCoroutine ("predictBallArival");
        StopAllCoroutines();
    }

    //	//Temp arrangement to throw the ball
    //	public void throwBall(){
    //
    //		GameManager.Ins.ball.throwBall (ballDirectorTrans, fireStrength, angularVelo);
    ////		FindObjectOfType<ThrowSimulationBall>().throwBall(FindObjectOfType<BezierTestMono>().bezier);
    //
    ////		ball.transform.position = transform.position;
    ////		ball.GetComponent<Rigidbody> ().velocity = (transform.up) * fireStrength * adjustmentFactor;
    ////		ball.GetComponent<Rigidbody> ().AddTorque (angulatVelo);
    ////		ball.GetComponent<Rigidbody> ().useGravity = true;
    //		if(ballHitTrans == null){
    //			ballHitTrans = GameObject.Find ("BallHitCube").transform;
    //		}
    //
    //		float zDistance = ballHitTrans.position.z - GameManager.Ins.ball.transform.position.z;
    //		float timeToReach = zDistance / GameManager.Ins.ball.gameObject.GetComponent<Rigidbody> ().velocity.z;
    //
    ////		Common.ProvideDelay(this, timeToReach, ()=>{
    ////			//Test Lines
    ////			//TestSliderScript.Ins.setTimeScele(0f);
    ////			StartCoroutine(CO_BallPauseAtHitPos());
    ////		});
    //
    //		GameManager.Ins.ball.StartCoroutine ("CO_BallPauseAtHitPos", timeToReach);
    //
    //		StartCoroutine (predictBallArival ());
    //	}

    ThrowSimulationBall throwBallSim = null;
    BallPathCreator ballPathCreator;
    public void throwBall()
    {

        if (throwBallSim == null)
        {
            throwBallSim = FindObjectOfType<ThrowSimulationBall>();
        }

        if (ballPathCreator == null)
        {
            ballPathCreator = FindObjectOfType<BallPathCreator>();
        }

        CurvePath ballPath = ballPathCreator.getRandomCurvePath(GameManager.Ins.isMagicBall); ; //getCurvePathWithName ("Path1");

#if UNITY_EDITOR
        if (ballPath == null)
        {
            Debug.LogError("Path Not Found");
            return;
        }
#endif

        // Debug.Log(ballPath.p1 + " p1");
        // Debug.Log(ballPath.p2 + " p2");
        // Debug.Log(ballPath.p3 + " p3");
        throwBallSim.throwBall(ballPath);

        StartCoroutine(predictBallArival());
        //		Debug.Log ("Time To Reach:" + throwBallSim.totalTrevalTimeInSec);
    }


    public void swingBat()
    {
        //		TestSliderScript.Ins.setTimeScele (0.1f);
        batterAnimator.SetTrigger("Swing");
    }

    [SerializeField] GameObject predictImg;
    [SerializeField] Transform throwDumyBallTrans;
    IEnumerator predictBallArival()
    {
        predictImg.transform.localScale = Vector3.one;


        float initialDistanceZ = ballHitTrans.position.z - throwDumyBallTrans.position.z; //GameManager.Ins.ball.transform.position.z;

        yield return null;

        float disZ;
        float scaleFact;
        while (true)
        {
            disZ = ballHitTrans.position.z - throwDumyBallTrans.position.z; //GameManager.Ins.ball.transform.position.z;
            scaleFact = disZ / initialDistanceZ;

            predictImg.transform.localScale = new Vector3(scaleFact, scaleFact, scaleFact);

            if (scaleFact > 0)
            {
                yield return null;
            }
            else
            {
                yield break;
            }
        }
    }
}
