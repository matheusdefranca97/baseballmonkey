﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusCoinRotate : MonoBehaviour {


	public Text WheelCoinCount;
	public Text bonusCount;
	public Text totalBonusCount;
	public Transform wheelCoin;

	Vector3 rot;
	float rotationAmount ;//= 360f;//= 260f;
	float rotationAmount_End = 0f ;//= 260f;
	void Start () {

//		rotationAmount = Random.Range(8,16)*45f + 22.5f;
//		Debug.Log("rotationAmount : "+rotationAmount);
//		rot = wheelCoin.transform.rotation.eulerAngles;
//
//		StartCoroutine("CO_Rotate");

	}

	public void wheelStart()
	{
		rotationAmount = Random.Range(8,16)*45f + 22.5f;
		Debug.Log("rotationAmount : "+rotationAmount);
		rot = Vector3.zero;
		wheelCoin.transform.rotation = Quaternion.Euler(rot);

		StartCoroutine("CO_Rotate");		
	}

	float speed = 500f;

	IEnumerator CO_Rotate()
	{
		float localRotation = rotationAmount;
		Debug.Log("**TEMP: " + localRotation);

		while(localRotation>0)
		{
			localRotation = localRotation - Time.deltaTime*speed;


			rot.z = rot.z + Time.deltaTime*speed;	
			wheelCoin.transform.rotation = Quaternion.Euler(rot);
			yield return null;
		}

		//yield return null;

		float tempValue = (rotationAmount) % 360 + 135f + 5f;
		tempValue = (tempValue / 45f) % 8 + 1;
		Debug.Log("Coins Multi: " + tempValue);
		GameData.wheelSpinCount = (int)tempValue;
		int ScoreCalculate = GameData.wheelSpinCount * GameData.bonusCount * 100;
		GameData.extraScoreByBonusWheel = ScoreCalculate;
		Debug.Log("GameData.wheelSpinCount :"+GameData.wheelSpinCount+"GameData.bonusCount :"+GameData.bonusCount);

		bonusCount.text = GameData.bonusCount.ToString()+" X "+GameData.wheelSpinCount.ToString() + "X 100 : "+ ScoreCalculate.ToString();
	//	totalBonusCount.text = ScoreCalculate.ToString();
		yield return new WaitForSeconds(3f);
		this.gameObject.SetActive(false);
		bonusCount.text = "";
		rotationAmount = 0f;
//		for (byte i = 0; i < SceneCanvasMgr.Ins.canvases.Length; i++) {
//			SceneCanvasMgr.Ins.canvases [i].canvas.SetActive (false);
//		}

	}

}
