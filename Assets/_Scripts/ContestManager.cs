﻿using UnityEngine;
using System.Collections;

public class Contest{
	public string contest_id;//": "15",
	public string contest_name;//": "Monkey Contest 11",
	public string start_date;//": "2017-11-17",
	public string end_date;//": "2017-11-24",
	public string coins;//": "20",
	public string coins_name;//": "RaffleCoins",
	public string prize;//": "Holiday Package",
	public string image;//": "/home/cubicot8/public_html/monkey/wp-content/uploads/contest/download.jpg",
	public string description;//": "Test",
	public string broker_key;//": ""
}

public class ContestManager : MonoBehaviour {

	[SerializeField]
	GameObject loadingPanel;
	[SerializeField]
	GameObject contestsScroll;
	[SerializeField]
	GameObject contestsParent;
	[SerializeField]
	GameObject contestsPrefab;
//	[SerializeField]
//	GameObject ContestPAnel;


	public void showContestsClicked(){
		Debug.Log("In GetContest URL 1111");
		if (NetChecker.IsNetAvail) {
		//	Debug.Log("In GetContest URL 2222");
		//Amit 29/11/17 	PopUpsManager.ins.openScreen (MyScreens.Contest);
			ScenePanelMgr.Ins.showPanel(PanelType.Contest_Panel);
			//remove old childs
			for(int i=0;i<contestsParent.transform.childCount ; i++){
				Destroy (contestsParent.transform.GetChild(i).gameObject);
			}

			// Amit 29/11/17 Player pl = MyPrefs.ins.getPlayer ();
				Player pl = Player.getPlayer();
			if (pl != null && !string.IsNullOrEmpty (pl.pId)) {
				//get current conversion values
				Debug.Log("pl.pId : "+ pl.pId);
			//	DataInserter.Din.GetAllContests("142"/*pl.pId*/,""/*pl.pBorkerKey*/,OnContestsGotCallback);
				DataInserter.Din.GetAllContests(pl.pId,pl.pBorkerKey,OnContestsGotCallback);
//				DataInserter.Din.GetAllContests(pl.pId,"va123",OnContestsGotCallback);
				loadingPanel.SetActive (true);
				contestsScroll.SetActive (false);
			} else {
				//Debug.Log("Id is null :"+pl.pId);
				//Amit 29/11/17PopUpsManager.ins.openScreen (MyScreens.ExchangeNotLogin);	
				InfoPopup.Ins.showSingleBtnInfoPopup("There are no contests..", "OK");
				Debug.Log("pl.pId : "+ pl.pId);
				Debug.Log("No Contest");
			}			
		} else
			//Amit 29/11/17PopUpsManager.ins.openInternetPanel ();
			InfoPopup.Ins.showSingleBtnInfoPopup("Oops, It seems you are not connected with internet..\\n\\nPlease check..", "OK");
		return;
	}

	// [{"contest_id":"15","contest_name":"Monkey Contest 11","start_date":"2017-11-17","end_date":"2017-11-24","coins":"20","coins_name":"RaffleCoins","prize":"Holiday Package","image":"/home/cubicot8/public_html/monkey/wp-content/uploads/contest/download.jpg","description":"Test","broker_key":""},{"contest_id":"16","contest_name":"Contest","start_date":"2017-11-24","end_date":"2017-11-15","coins":"100000","coins_name":"MonkeyCoins","prize":"Test","image":"/home/cubicot8/public_html/monkey/wp-content/uploads/contest/6ed61ee28ac6c5d0a547a04bc4a22b37.jpg","description":"Test","broker_key":""},{"contest_id":"18","contest_name":"Contest 3","start_date":"2017-11-08","end_date":"2017-11-09","coins":"10","coins_name":"RaffleCoins","prize":"Test","image":"http://cubicalhub.com/monkey/wp-content/uploads/contest/php.jpg","description":"Test","broker_key":""}]
	void OnContestsGotCallback(WWW www){

		if (string.IsNullOrEmpty (www.error)) {
			loadingPanel.SetActive (false);
			contestsScroll.SetActive (true);
			//show them on contests ui
			print (" Contests : " + www.text);

			JSONObject response = new JSONObject (www.text);
			if (response != null) {
				for (int i = 0; i < response.Count; i++) {
					Contest nContest = JsonUtility.FromJson<Contest>(response [i].ToString ());
					GameObject go = Instantiate (contestsPrefab) as GameObject;
					go.transform.SetParent (contestsParent.transform);
					go.transform.localScale = Vector3.one;
					go.transform.position = Vector3.zero;
					go.transform.GetComponent<ContestUI> ().setContest (nContest);
					string user_register = response[i].GetField("register_user").ToString().Replace("\"","");
					bool flag;
					if (bool.TryParse(user_register, out flag))
					{
						//Debug.Log("flag :"+flag);
						if(flag == true)
						{
							go.transform.GetComponent<ContestUI>().joindButton.SetActive(false);
							go.transform.GetComponent<ContestUI>().alreadyJoind.SetActive(true);
						}else{
							go.transform.GetComponent<ContestUI>().joindButton.SetActive(true);
							go.transform.GetComponent<ContestUI>().alreadyJoind.SetActive(false);
						}
						//go.transform.GetComponent<ContestUI>().is_joindContest = flag;
					}

				}
			}
		} else
			InfoPopup.Ins.showSingleBtnInfoPopup("Could not get data ...", "OK");
		return;
			//Amit 29/11/17PopUpsManager.ins.showSingleButtonPopUp ("Could not get data ...");
	}
}
