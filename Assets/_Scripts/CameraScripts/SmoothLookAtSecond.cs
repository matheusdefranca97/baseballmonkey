﻿using UnityEngine;
using System.Collections;

///<summary>
///Looks at a target
///</summary>
[AddComponentMenu("Camera-Control/Smooth Look At CS")]
public class SmoothLookAtSecond : MonoBehaviour
{
	public Transform target;        //an Object to lock on to
	public float damping = 6.0f;    //to control the rotation 
	public bool smooth = true;
	public float minDistance = 10.0f;   //How far the target is from the camera
	public string property = "";

	private Color color;
	private float alpha = 1.0f;
	public static Transform _myTransform;
	[SerializeField]
	public static Vector3 saveSecondCameraPosition;
	//public static Quaternion saveCameraRotation;


	void Awake()
	{
		_myTransform = transform;

	}

	void LateUpdate()
	{
		if (target)
		{
			if (smooth)
			{
				//Debug.Log ("IN SMOOTH field is TRUE   IF COnDITION  "+smooth);
				//Look at and dampen the rotation
				Quaternion rotation = Quaternion.LookRotation(target.position - _myTransform.position);
				_myTransform.rotation = Quaternion.Slerp(_myTransform.rotation, rotation, Time.deltaTime * damping);

				transform.position = Vector3.MoveTowards(transform.position, target.position, damping * Time.deltaTime);
				saveSecondCameraPosition = transform.position;

			}
			else
			{ //Just look at
				Debug.Log("else else else  " + smooth);
				_myTransform.rotation = Quaternion.FromToRotation(-Vector3.forward, (new Vector3(target.position.x, target.position.y, target.position.z) - _myTransform.position).normalized);

				float distance = Vector3.Distance(target.position, _myTransform.position);

				if (distance < minDistance)
				{

					alpha = Mathf.Lerp(alpha, 0.0f, Time.deltaTime * 2.0f);
				}
				else
				{

					alpha = Mathf.Lerp(alpha, 1.0f, Time.deltaTime * 2.0f);

				}
				//				if(!string.IsNullOrEmpty(property)) {
				//					color.a = Mathf.Clamp(alpha, 0.0f, 1.0f);

				//					renderer.material.SetColor(property, color);

				//				}
			}
		}
	}
}