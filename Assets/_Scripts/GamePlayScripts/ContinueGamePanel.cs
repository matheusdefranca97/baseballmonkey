﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class ContinueGamePanel : MonoBehaviour
{

	[SerializeField] Text txtMsg;

	Action yesBtnCallBack, cancelBtnCallBack;

	public void showContinuePopup(Action btnYesCallback, Action btnCancelCallback)
	{
		this.gameObject.SetActive(true);

		this.yesBtnCallBack = btnYesCallback;
		this.cancelBtnCallBack = btnCancelCallback;

		if (GameData.gameModeType == GameModeType.Classic)
		{
			txtMsg.text = "Want to have " + Common.getBonusBallToThrowForMode(GameData.gameModeType) + " more pitches?";
		}
		else if (GameData.gameModeType == GameModeType.OneOut)
		{
			txtMsg.text = "Do you want to continue playing?";
		}
		else if (GameData.gameModeType == GameModeType.Speed)
		{
			txtMsg.text = "Want to have " + Common.getBonusBallToThrowForMode(GameData.gameModeType) + " more pitches?";
		}
	}


	/// <summary>
	/// Buttons the yes clicked.
	/// </summary>
	public void BtnYesClicked()
	{

		gameObject.SetActive(false);

		if (yesBtnCallBack == null)
			return;
		yesBtnCallBack();
	}

	/// <summary>
	/// Buttons the cancel clicked.
	/// </summary>
	public void BtnCancelClicked()
	{
		gameObject.SetActive(false);

		if (cancelBtnCallBack == null)
			return;
		cancelBtnCallBack();

	}


}
