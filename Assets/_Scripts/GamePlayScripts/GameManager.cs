﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Game statistic variables
public static class GameData
{
    public static int totalScore = 0;   //its a length score for classic mode and Homerun count for OneOut and Speed Mode
    public static int ballLeftToThrow = 0;
    public static GameModeType gameModeType = GameModeType.None;
    public static float delayTimeBwtBallThrow;


    public static int homerunCount;
    public static int longestShotLength;

    public static int bestComboValue = 1;

    public static int magicBallThrowCount;
    public static int magicBallHitCount;

    public static int perfectHomerunCount;
    public static int greatHomerunCount;
    public static int nicehomerunCount;

    public static int bonusCount = 0;
    public static int wheelSpinCount = 0;
    public static int extraScoreByBonusWheel = 0;

    public static int multiplicator;


}

public class GameManager : MonoBehaviour
{

    [HideInInspector] public CharAnimCtrl CharacterMenuAni;
    //HomePopup _homePopup;
    //public GameObject homePanel;

    [SerializeField] GameObject mainCamera;
    //[SerializeField] GameObject ballCamera;
    [SerializeField] Text txtBallResult;

    //public GameObject batObject;
    //public Renderer rend;
    bool isBonusGameRunning = false;


    [HideInInspector] public Ball ball;
    public Animator animator; // Amit 17/5
    [SerializeField] GameObject secondBAllCamera;//Amit 
    public Transform firstCameraResetPoint;     // Amit 18/5
    public Transform secondCameraResetPoint;    // Amit 18/5
    public Transform ThirdCameraResetPoint;     // Amit 18/5
    int rndomCameraPosition;    //Amit 18/5
                                //	[SerializeField] Text txtTotalScore;

    //UI References
    [SerializeField] GameOverPopup gameOverPopup;
    [SerializeField] ContinueGamePanel continueGamePopup;
    [SerializeField] GamePlayUIPanel gamePlayUIPanel;

    public static GameManager Ins = null;
    void Awake()
    {

#if UNITY_EDITOR
        Debug.Assert(Ins == null);
#endif

        Ins = this;

        AppManager.EvtGameStart += GameStarted;
    }

    void OnDestroy()
    {
        AppManager.EvtGameStart -= GameStarted;
    }

    // Use this for initialization
    void Start()
    {
        //rend = GetComponent<Renderer>();
        //rend.enabled = true;

        CharacterMenuAni = FindObjectOfType<CharAnimCtrl>();
        ball = FindObjectOfType<Ball>();
        //batSelectionHold = FindObjectOfType<Drag2> ();
        //ball = FindObjectOfType<Ball> ();
        //gamePlayUIPanel = FindObjectOfType<GamePlayUIPanel> ();
        rndomCameraPosition = 0;    //Amit 18/5
                                    //hideBallCamera ();
        hideBallCameraSecond();
        //	_homePopup = GameObject.Find("HomePanel").GetComponent<HomePopup>();
    }

    private void Update()
    {
        // Debug.Log("isMagicBall?" + isMagicBall);
        // Debug.Log("multiplicator" + GameData.multiplicator);
    }

    public void backToHome()
    {
        StopAllCoroutines();
        playerFire.forcePitcherToIdle();
        ball.resetBallRigidBody();
    }

    //Method will be called from GameOver popup, On restart button click
    public void restartGame()
    {
        GameStarted();
    }

    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void Resume()
    {
        Time.timeScale = 1;
    }

    //Method is called on Game Start Event in App manager
    void GameStarted()
    {
        if (CharacterMenuAni != null)
            CharacterMenuAni.startGamePlayAnim();
        txtBallResult.text = "";
        txtShotLength.text = "";
        GameData.totalScore = 0;
        GameData.homerunCount = 0;
        GameData.longestShotLength = 0;
        GameData.magicBallThrowCount = 0;
        GameData.magicBallHitCount = 0;
        GameData.perfectHomerunCount = 0;
        GameData.greatHomerunCount = 0;
        GameData.nicehomerunCount = 0;
        GameData.bonusCount = 0;
        GameData.wheelSpinCount = 0;
        GameData.extraScoreByBonusWheel = 0;

        gamePlayUIPanel.setTotalScoreText();
        gamePlayUIPanel.setHomerunCountText();
        gamePlayUIPanel.setLongestShotText();
        gamePlayUIPanel.resetAllBallsStatus();

        shouldCalculateShotLength = false;
        StartBallThrowSeqCoroutineCount = 0;
        combo = 0;
        isBonusGameRunning = false;

        //init Game data accoring to game mode
        GameData.ballLeftToThrow = Common.getBallToThrowforMode(GameData.gameModeType);
        GameData.delayTimeBwtBallThrow = Common.getDelayTimeBwtBallsThrow(GameData.gameModeType);
        //Debug.Log("Delay Time Bwt Throw: " + GameData.delayTimeBwtBallThrow);

        if (playerFire == null)
        {
            playerFire = FindObjectOfType<PlayerFire>();
        }

        gamePlayUIPanel.setUIAccordingToMode();

        StartCoroutine(CO_StartBallThrowSeq());


        //	characterChange ();
    }

    PlayerFire playerFire;
    byte StartBallThrowSeqCoroutineCount = 0;
    public IEnumerator CO_StartBallThrowSeq()
    {

        StartBallThrowSeqCoroutineCount++;
        if (StartBallThrowSeqCoroutineCount == 1)
        {
            //First time Coroutine started
            yield return new WaitForSeconds(4f);
        }

        do
        {

            isCurrentBallResultOut = false;

            setNextBallType();
            if (isMagicBall)
            {
                //If nect Ball type is Special
                playerFire.showSpecialBallEffectOnPitcher();
                yield return new WaitForSeconds(1f);
            }

            //			playerFire.animator.SetTrigger ("Throw");
            playerFire.runBallThrowAnimationOnPitcher();

            yield return new WaitUntil(() => isCurrentBallResultOut == true);

            if (!(GameData.gameModeType == GameModeType.OneOut && currentBallREsultType != BallResultType.Strike))
            {
                GameData.ballLeftToThrow--;
                //Debug.Log("DECREMENT: " + GameData.ballLeftToThrow);
            }
            showBallResultOnUI(currentBallREsultType);

            yield return new WaitForSeconds(GameData.delayTimeBwtBallThrow);

        } while (GameData.ballLeftToThrow > 0);


        //Show Game Result Popup after all throw balls are vanished
        yield return new WaitForSeconds(1f);    //wait for some time bafore displaying the result
        if (shouldCalculateShotLength)
        {
            shouldCalculateShotLength = false;
            yield return new WaitForSeconds(1f);
        }


        // if (StartBallThrowSeqCoroutineCount < 2)
        // {
        //     continueGamePopup.showContinuePopup(() =>
        //     {
        //         GameData.ballLeftToThrow = Common.getBonusBallToThrowForMode(GameData.gameModeType);// it will be decided by Game mode type
        //         isBonusGameRunning = true;

        //         //for Speed mode, we need to set the left ball count number before bonus game start
        //         if (GameData.gameModeType == GameModeType.Speed)
        //         {
        //             gamePlayUIPanel.setBallStatusForSpeed(GameData.ballLeftToThrow);
        //         }

        //         StartCoroutine(CO_StartBallThrowSeq());
        //     }, () =>
        //     {
        //         gameOverPopup.showGameOverPopup();
        //     });
        // }
        // else
        // {
        gameOverPopup.showGameOverPopup();
        //}

    }


    void showBallResultOnUI(BallResultType ballResultTyp)
    {
        if (GameData.gameModeType == GameModeType.Classic)
        {
            gamePlayUIPanel.setBallStatusForClassicMode(GameData.ballLeftToThrow, ballResultTyp, isBonusGameRunning);
        }
        else if (GameData.gameModeType == GameModeType.OneOut)
        {
            gamePlayUIPanel.setBallStatusForOneOut(GameData.ballLeftToThrow, ballResultTyp, isBonusGameRunning);
        }
        else if (GameData.gameModeType == GameModeType.Speed)
        {
            gamePlayUIPanel.setBallStatusForSpeed(GameData.ballLeftToThrow);
        }
    }

    /*public void showBallCamera(){
		ballCamera.SetActive (true);
		mainCamera.SetActive (false);

//		Time.timeScale = 0.5f;

		StartCoroutine (Common.CO_ProvideDelay (3f, () => {
			Debug.Log ("Ball -> Main Camera");
			hideBallCamera ();
		}));
	}*/

    /*public void hideBallCamera(){
		mainCamera.SetActive (true);
		ballCamera.SetActive (false);

//		Time.timeScale = 1f;
	}*/


    public void showBallCameraSecond()
    {
        secondBAllCamera.SetActive(true);
        mainCamera.SetActive(false);

        //		Time.timeScale = 0.5f;

        StartCoroutine(Common.CO_ProvideDelay(3f, () =>
        {
            Debug.Log("Ball -> Main Camera");
            hideBallCameraSecond();
        }));
    }

    public void hideBallCameraSecond()
    {


        //Debug.Log("hidei");
        rndomCameraPosition = Random.Range(1, 3);

        // Debug.Log("rndomCameraPosition" + rndomCameraPosition);

        switch (rndomCameraPosition)
        {
            case 1:
                secondBAllCamera.transform.position = firstCameraResetPoint.transform.position; //Amit 18/5
                secondBAllCamera.transform.rotation = firstCameraResetPoint.transform.rotation;
                break;

            case 2:
                secondBAllCamera.transform.position = secondCameraResetPoint.transform.position;    //Amit 18/5
                secondBAllCamera.transform.rotation = secondCameraResetPoint.transform.rotation;
                break;

            case 3:
                secondBAllCamera.transform.position = ThirdCameraResetPoint.transform.position; //Amit 18/5
                secondBAllCamera.transform.rotation = ThirdCameraResetPoint.transform.rotation;
                break;

        }


        mainCamera.SetActive(true);
        secondBAllCamera.SetActive(false);

        //		Time.timeScale = 1f;
    }


    private bool isCurrentBallResultOut = false;
    private BallResultType currentBallREsultType = BallResultType.None;
    private BallResultSubType currentBallREsultSubType = BallResultSubType.None;//Amit 17/5

    private int combo;
    public void showBallResult(BallResultType type)
    {

        if (type == BallResultType.Strike)
        {
            txtBallResult.text = "Strike";
            txtBallResult.color = Color.red;
            resetCombo();
        }
        else if (type == BallResultType.Foul)
        {
            txtBallResult.text = "Foul";
            txtBallResult.color = Color.grey;
            resetCombo();
        }
        else if (type == BallResultType.HomeRun)
        {
            txtBallResult.text = "Home Run";
            txtBallResult.color = Color.green;
            GameData.homerunCount++;
            gamePlayUIPanel.setHomerunCountText();
            StartCoroutine(CO_calculateShotLength());
            incrementAndSetCombo();
            //	showBallCameraSecond ();
            animator.Play("CameraVibrateAniamtion 1");//Amit 18/5

            if (isMagicBall)
            {
                GameData.multiplicator = 5;
                //if its a magic ball then maintain "Magic Ball hit" count
                GameData.magicBallHitCount++;
            }

        }
        else if (type == BallResultType.None)
        {
            txtBallResult.text = "Hit";
            txtBallResult.color = Color.yellow;
            resetCombo();
            //Do noting
        }

        //		Debug.Log ("@@@ Ball Result: GameData.ballLeftToThrow: " + GameData.ballLeftToThrow);
        //		TestSliderScript.Ins.setTimeScele (0f);

        StartCoroutine(Common.CO_ProvideDelay(1f, () =>
        {
            txtBallResult.text = "";
        }));

        currentBallREsultType = type;
        isCurrentBallResultOut = true;
    }

    //Amit...Start

    public void showBallResultSub(BallResultSubType type)//Amit 17/5
    {
        if (type == BallResultSubType.Great)
        {
            if (GameData.gameModeType == GameModeType.Classic)
                GameManager.Ins.showBallCameraSecond();//Amit 18/5
            GameData.multiplicator = 3;
            txtBallResult.text = "Great";

            GameData.greatHomerunCount++;

            //	Debug.Log ("D444444444");
            //		StartCoroutine(CO_calculateShotLength());
            //GameManager.Ins.showBallCamera ();//Amit 18/5

        }
        else if (type == BallResultSubType.Perfact)
        {
            if (GameData.gameModeType == GameModeType.Classic)
                GameManager.Ins.showBallCameraSecond();//Amit 18/5
            txtBallResult.text = "Perfect";
            GameData.multiplicator = 3;

            GameData.perfectHomerunCount++;

            //		StartCoroutine(CO_calculateShotLength());

            //GameManager.Ins.showBallCamera ();//Amit 18/5
            //resetCombo ();

        }
        else if (type == BallResultSubType.Nice)
        {
            //	GameManager.Ins.showBallCameraSecond ();//Amit 18/5
            txtBallResult.text = "Nice";

            GameData.nicehomerunCount++;

            //	Debug.Log ("f66666666");
            //		StartCoroutine(CO_calculateShotLength());

            //resetCombo ();

        }
        else if (type == BallResultSubType.Simple)
        {
            //	GameManager.Ins.showBallCameraSecond ();//Amit 18/5
            txtBallResult.text = "Good";

            //	Debug.Log ("G77777777");
            //		StartCoroutine(CO_calculateShotLength());


            //resetCombo ();
            //Do noting
        }
        else if (type == BallResultSubType.None)
        {
            Debug.Log("n sei");
            //Do noting
        }

        //		Debug.Log ("@@@ Ball Result: GameData.ballLeftToThrow: " + GameData.ballLeftToThrow);
        //		TestSliderScript.Ins.setTimeScele (0f);


        //	StartCoroutine (Common.CO_ProvideDelay (1f, () => {
        //		txtBallResult.text = "";
        //	}));

        currentBallREsultSubType = type;

    }

    //Amit...END


    [SerializeField] Text txtShotLength;
    Transform homePlateTrans;
    [HideInInspector] public bool shouldCalculateShotLength;
    [SerializeField] int lengthMultiFact;
    //Coroutine to count the shot length
    IEnumerator CO_calculateShotLength()
    {

        shouldCalculateShotLength = true;

        if (homePlateTrans == null)
        {
            homePlateTrans = (GameObject.Find("HomePlate") as GameObject).transform;
        }

        int shotLength = 0;

        //		int count = 0;
        while (shouldCalculateShotLength)
        {
            int updatedLength = (int)(new Vector2(homePlateTrans.position.x, homePlateTrans.position.z) -
                new Vector2(ball.transform.position.x, ball.transform.position.z)).magnitude * lengthMultiFact;


            if (updatedLength > shotLength)
            {
                shotLength = updatedLength;

            }



            txtShotLength.text = shotLength.ToString();

            yield return null;
            //			count++;
            //			Debug.Log ("shotLength: " + shotLength);
        }

        if (GameData.gameModeType == GameModeType.Classic)
        {



            GameData.totalScore += (shotLength * GameData.multiplicator);
            //Debug.Log("aqui que eu calculo");
            Debug.Log(currentBallREsultSubType);
            GameData.multiplicator = 1;


        }
        else if (GameData.gameModeType == GameModeType.OneOut)
        {
            GameData.totalScore = GameData.homerunCount;
        }
        else if (GameData.gameModeType == GameModeType.Speed)
        {
            GameData.totalScore = GameData.homerunCount;
        }

        yield return new WaitForSeconds(1f);

        gamePlayUIPanel.setTotalScoreText();
        txtShotLength.text = "";

        if (GameData.longestShotLength < shotLength)
        {
            GameData.longestShotLength = shotLength;
            gamePlayUIPanel.setLongestShotText();
        }

    }

    void resetCombo()
    {
        combo = 0;
        gamePlayUIPanel.hideCombo();
    }

    void incrementAndSetCombo()
    {
        combo++;

        if (combo > GameData.bestComboValue)
        {
            GameData.bestComboValue = combo;
        }

        if (combo > 1)
        {
            gamePlayUIPanel.showCombo(combo);
        }
    }

    [HideInInspector] public bool isMagicBall = false;
    public void setNextBallType()
    {
        if (GameData.magicBallThrowCount == 0)
        {
            isMagicBall = (Random.Range(0, GameData.ballLeftToThrow + 1) < (GameData.ballLeftToThrow + 1) / 4 ? true : false);
        }
        else
        {
            isMagicBall = (Random.Range(0, 100) < 10 ? true : false);
        }

        if (isMagicBall)
        {
            GameData.magicBallThrowCount++;
        }

#if UNITY_EDITOR
        //Debug.Log("GameData.specialBallThrowCount : " + GameData.magicBallThrowCount);
#endif
    }


    /*void characterChange()
	{
	if(homePanel.){
			Debug.Log ("Menu Character");
		} else {
		Debug.Log ("Main Player");
		}
	}*/





}
