﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

	[SerializeField] float forceValue = 1f;
	[SerializeField][Range(5f, 10f)] float batHitStrangth = 3f;
	//	[SerializeField] Transform ballPosTrans;
	[SerializeField] GameObject ballTrail;

	Vector3 initialPos;
	Rigidbody rigidBody;
	public float randomRangeForce;

	void Start()
	{

		initialPos = transform.position;
		rigidBody = GetComponent<Rigidbody>();
		ballTrail = GetComponentInChildren<TrailRenderer>().gameObject;

		resetBallRigidBody();
	}

	bool isBat = false;
	//	public void throwBall(Transform ballThrowerTrans, float strengthFactor, Vector3 angulatVelo){
	//
	//		resetBallRigidBody ();
	//
	//		transform.position = ballThrowerTrans.position;
	//		rigidBody.velocity = (ballThrowerTrans.transform.up) * strengthFactor;
	//		rigidBody.AddTorque (angulatVelo);
	//		rigidBody.useGravity = true;
	//
	//		isBat = false;
	//	}

	public void placeBallAtHitPos(Vector3 hitPos)
	{
		isBat = false;  //Ball is readt to get hit by bat
		transform.position = hitPos;
		StartCoroutine("CO_BallPauseAtHitPos", 0);
	}

	IEnumerator CO_BallPauseAtHitPos(float timeToReach)
	{
		//Debug.Log(timeToReach + "timetoreach");
		yield return new WaitForSeconds(timeToReach);

		rigidBody.useGravity = false;
		rigidBody.velocity = Vector3.zero;
		rigidBody.angularVelocity = Vector3.zero;

		yield return new WaitForSeconds(0.18f);

		//		Debug.Log ("isBat: " + isBat);
		if (isBat == false)
		{
			checkForHomeRunType(Vector3.zero, false);
			resetBallRigidBody();
		}
	}

	//void OnCollisionEnter(Collision collision)
	//{

	//    Debug.Log("CollisionEnter1: " + collision.gameObject.name);
	//    if (collision.gameObject.tag == "Bat" && isBat == false)
	//    {
	//        isBat = true;
	//        ballTrail.SetActive(true);
	//        Debug.Log("CollisionEnter2");
	//        Vector3 forceDir = (transform.position - collision.contacts[0].point).normalized;
	//        Vector3 forceVect = forceDir * forceValue;

	//        float forceY = (forceVect.y < 0 ? forceVect.y * -1 : forceVect.y);
	//        forceY = Mathf.Clamp(forceY, 0, 2f);
	//        forceVect = new Vector3(Random.Range(1.5f, 3f) + forceVect.x, forceY, (forceVect.z > 0 ? forceVect.z * -1 : forceVect.z));

	//        rigidBody.AddForce(forceVect, ForceMode.Impulse);
	//        rigidBody.useGravity = true;

	//        //			StartCoroutine (resetPos ());

	//        //			TestSliderScript.Ins.setTimeScele (0.001f);

	//        Debug.Log("Force Vect: " + forceVect);
	//        Debug.Log("Force Magnitude: " + forceVect.magnitude);

	//        checkForHomeRunType(forceVect, true);
	//        //			GameManager.Ins.showBallCamera ();
	//    }

	//}

	[SerializeField] GameObject batHitBallEffectPrefab;
	void OnTriggerEnter(Collider other)
	{

		//Debug.Log("TriggerEnter1: " + other.gameObject.name);
		if (other.gameObject.tag == "Bat" && isBat == false)
		{

			BottomOptional.vibrateMethod();

			GameObject hitEffect = GameObject.Instantiate(batHitBallEffectPrefab) as GameObject;
			hitEffect.transform.position = transform.position;  //Assign ball possion to the hit effect

			StartCoroutine(Common.CO_ProvideDelay(0.5f, () =>
			{
				Destroy(hitEffect);
			}));

			isBat = true;
			//			TestSliderScript.Ins.setTimeScele (0f);
			ballTrail.SetActive(true);
			//			StopCoroutine ("CO_BallPauseAtHitPos");
			//Debug.Log("CollisionEnter2");
			Vector3 forceDir = (transform.position - other.transform.position).normalized; //contacts [0].point).normalized;

			Vector3 forceVect = forceDir * forceValue;
			float forceY = (forceVect.y < 0 ? forceVect.y * -1 : forceVect.y) + 3f;
			forceY = Mathf.Clamp(forceY, 3f, 5f);

			forceVect = new Vector3(forceVect.x, forceY, (forceVect.z > 0 ? forceVect.z * -1 : forceVect.z));
			forceVect.x = Random.Range(-8f, 8f);


			randomRangeForce = Random.Range(3.5f, 11.5f);

			forceVect = (forceVect.normalized * randomRangeForce); //batHitStrangth;

			rigidBody.AddForce(forceVect, ForceMode.Impulse);
			rigidBody.useGravity = true;

			//			StartCoroutine (resetPos ());

			//			TestSliderScript.Ins.setTimeScele (0.001f);

			//Debug.Log("Force Vect: " + forceVect);
			//Debug.Log("Force Magnitude: " + forceVect.magnitude);

			checkForHomeRunType(forceVect, true);
			//						GameManager.Ins.showBallCamera ();//Amit 22/5
		}
		else if (other.gameObject.tag == "Boundary")
		{
			Debug.Log("Ball: Boundary Name: " + other.gameObject.name);

			//Reset Ball Rigid Body
			resetBallRigidBody();

			//			rigidBody.velocity = Vector3.zero;
			//			rigidBody.angularVelocity = Vector3.zero;
			//			transform.position = initialPos;
		}
	}

	public void calculateForceDependsOnType(BallResultSubType type)
	{
		if (type == BallResultSubType.Great)
		{


		}
		else if (type == BallResultSubType.Perfact)
		{


		}
		else if (type == BallResultSubType.Nice)
		{

		}
		else if (type == BallResultSubType.Simple)
		{

			//	Debug.Log ("G77777777");
			//		StartCoroutine(CO_calculateShotLength());


			//resetCombo ();
			//Do noting

		}
	}

	//	IEnumerator resetPos(){
	//
	//		yield return new WaitForSeconds (5f);
	//
	//		transform.position = initialPos;
	////		rigidBody.useGravity = false;
	//		rigidBody.velocity = Vector3.zero;
	//		rigidBody.angularVelocity = Vector3.zero;
	//
	//		FindObjectOfType<Animator> ().SetTrigger ("Swing");
	//	}

	public void resetBallRigidBody()
	{
		ballTrail.SetActive(false);
		GameManager.Ins.shouldCalculateShotLength = false;
		transform.position = initialPos;
		rigidBody.velocity = Vector3.zero;
		rigidBody.AddTorque(Vector3.zero);
	}

	//	public void throwBall(Vector3 destination){
	////		destination = new Vector3 (-1.12f, 1.016f, 0.33f);
	//	}

	//[Header ("Ball Physics")]
	//[SerializeField] bool shouldApplyAirDrag = false;
	//[SerializeField] float fudgeFactor;
	//void FixedUpdate(){
	//	if (!shouldApplyAirDrag) {
	//		return;
	//	}

	//	rigidBody.AddForce( fudgeFactor*Vector3.Cross(rigidBody.velocity,rigidBody.angularVelocity), ForceMode.Force);

	//}

	//Method to check if there is Perfect Home Run Condition
	Animator battingPlyrAnimator = null;
	void checkForHomeRunType(Vector3 force, bool isBatHitBall)
	{

		if (battingPlyrAnimator == null)
		{
			battingPlyrAnimator = FindObjectOfType<PlayerFire>().batterAnimator;
		}

		AnimatorStateInfo info = battingPlyrAnimator.GetCurrentAnimatorStateInfo(0);
		//		if (info.IsName ("Swing")) {
		//			//if current state is Swing then
		//			Debug.Log ("Clip time: " + info.normalizedTime);
		//		}

		BallResultType ballResult = BallResultType.None;
		BallResultSubType ballResultSub = BallResultSubType.None;//Amit 17/5

		if (isBatHitBall)
		{
			//			Debug.Log ("Draw Line");
			//			Debug.DrawRay (transform.position, 10f * force, Color.red, 15f);
			//			Debug.DrawRay (transform.position, 10f * force.magnitude * Vector3.back, Color.green, 15f);

			float xAngle = Vector3.Angle(Vector3.back, new Vector3(force.x, 0, force.z));
			float yAngle = Vector3.Angle(Vector3.back, new Vector3(0, force.y, force.z));

#if UNITY_EDITOR
			//			Debug.Log ("Angle: X " + xAngle + ",Y " + yAngle);
			Debug.Assert(xAngle >= 0f);
#endif

			/*if (xAngle < 45f) {
				if (yAngle > 9.5f && yAngle < 50f && force.magnitude > 3.5f) {
//					GameManager.Ins.showBallResult (BallResultType.HomeRun);
					ballResult = BallResultType.HomeRun;
				}*/
			if (xAngle < 45f)
			{

				ballResult = BallResultType.HomeRun;
				float forceMagnitude = force.magnitude;
				//Debug.Log ("Force Magnitude: " + forceMagnitude);

				if (yAngle > 9.5f && yAngle < 50f && forceMagnitude < 6.5f)
				{
					//					GameManager.Ins.showBallResult (BallResultType.HomeRun);
					ballResultSub = BallResultSubType.Simple;
					AudioManager.ins.playEffectSound(AudioManager.ins.homerunHitClip);

				}
				else if (yAngle > 9.5f && yAngle < 50f && forceMagnitude < 6.8f)
				{
					ballResultSub = BallResultSubType.Nice;
					AudioManager.ins.playEffectSound(AudioManager.ins.homerunHitClip);

				}
				else if (yAngle > 9.5f && yAngle < 50f && forceMagnitude < 7f)
				{
					ballResultSub = BallResultSubType.Great;
					AudioManager.ins.playEffectSound(AudioManager.ins.homerunHitClip);

				}
				else if (yAngle > 9.5f && yAngle < 50f && forceMagnitude > 7.2f)
				{
					ballResultSub = BallResultSubType.Perfact;
					AudioManager.ins.playEffectSound(AudioManager.ins.homerunHitClip);

				}
				else
				{
					//DO nothing
					//Debug.Log("Home Run Else: ");
				}
			}
			else
			{
				//				GameManager.Ins.showBallResult (BallResultType.Foul);
				ballResult = BallResultType.Foul;
			}
		}
		else
		{
			//Bat not hit the ball	
			//			GameManager.Ins.showBallResult (BallResultType.Strike);
			ballResult = BallResultType.Strike;
			AudioManager.ins.playEffectSound(AudioManager.ins.strike);
		}

		GameManager.Ins.showBallResult(ballResult);
		if (ballResult == BallResultType.HomeRun)
		{
			GameManager.Ins.showBallResultSub(ballResultSub);//Amit 17/5
		}
	}


}
