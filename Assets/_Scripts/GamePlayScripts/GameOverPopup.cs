﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class GameOverPopup : MonoBehaviour
{

	[SerializeField] Text txtScoreValue;
	[SerializeField] Text txtLongestShot;

	[Header("Current Game Player Value Show Texts")]
	[SerializeField] Text txtCurrentHomerunOutOfTotal;
	[SerializeField] Text txtCurrentPerfectHomerun;
	[SerializeField] Text txtCurrentGreatHomerun;
	[SerializeField] Text txtCurrentNiceHomerun;
	[SerializeField] Text txtMagicBallHitOutOfTotal;

	[Header("current shots coins collected")]
	[SerializeField] Text txtHomerunCoins;
	[SerializeField] Text txtPrefectHomerunCoins;
	//[SerializeField] Text txtBonusCoins;
	[SerializeField] Text txtTotalCoins;


	[Header("Thirdweb")]
	[SerializeField] Button claimButton;
	[SerializeField] TextMeshProUGUI claimText;



	[Header("Buttons")]
	[SerializeField] Button rewardedAdd;





	void showValuesPopup()
	{

		//MOSTRAR AD
		//rewardedAdd.interactable = AppLovinMgr.Ins.IsRewardedAdLoaded;



		GameData.totalScore = GameData.totalScore;
		txtScoreValue.text = GameData.totalScore.ToString();
		txtLongestShot.text = GameData.longestShotLength.ToString();

		//Current Game Player Value Show
		if (GameData.gameModeType != GameModeType.OneOut)
		{
			txtCurrentHomerunOutOfTotal.text = GameData.homerunCount + "/" + Common.getBallToThrowforMode(GameData.gameModeType);
		}
		else
		{
			txtCurrentHomerunOutOfTotal.text = "" + GameData.homerunCount;
		}

		txtCurrentPerfectHomerun.text = "" + GameData.perfectHomerunCount;
		txtCurrentGreatHomerun.text = "" + GameData.greatHomerunCount;
		txtCurrentNiceHomerun.text = "" + GameData.nicehomerunCount;
		txtMagicBallHitOutOfTotal.text = GameData.magicBallHitCount + "/" + GameData.magicBallThrowCount;

		//Coins Display



		int homerunCoins = GameData.homerunCount * Common.getHomerunCoinsMultiplierForMode(GameData.gameModeType);
		txtHomerunCoins.text = "" + homerunCoins;

		int perfectShotCoins = GameData.perfectHomerunCount * Common.getPerfectHomerunCoinsMultiplierForMode(GameData.gameModeType);
		txtPrefectHomerunCoins.text = "+" + perfectShotCoins;

		//int bonusCoins = GameData.bonusCount * GameData.wheelSpinCount;
		int totalCoins = homerunCoins + perfectShotCoins + GameData.extraScoreByBonusWheel;
		//txtBonusCoins.text = "+" + bonusCoins; //For now, Bonus coins count is always ZERO

		txtTotalCoins.text = "" + totalCoins;


		//AllCoins coins = CoinsManager.ins.getUserCoinDetails();
		//coins.MonkeyCoins += (ulong)totalCoins;//ulong.Parse(txtTotalCoins.text);
		//CoinsManager.ins.saveUserCoins(coins);
		//PlayerPrefs.SetInt (Common.Key_SimpleCoinsCount, PlayerPrefs.GetInt(Common.Key_SimpleCoinsCount, 0) + int.Parse(txtTotalCoins.text));
	}

	public void showGameOverPopup()
	{
		gameObject.SetActive(true);
		showValuesPopup();

		PlayerLevelPowerHandler.calculateLevel(GameData.homerunCount, GameData.perfectHomerunCount, GameData.greatHomerunCount, GameData.nicehomerunCount);

		string preferenceKey = "";
		if (GameData.gameModeType == GameModeType.Classic)
		{
			preferenceKey = Common.Key_BestScore_Classic;
		}
		else if (GameData.gameModeType == GameModeType.OneOut)
		{
			preferenceKey = Common.Key_BestScore_OneOut;
		}
		else if (GameData.gameModeType == GameModeType.Speed)
		{
			preferenceKey = Common.key_BestScore_Speed;
		}

		//Set player preference data
		int savedBestScore = PlayerPrefs.GetInt(preferenceKey, 0);
		if (GameData.totalScore > savedBestScore)
		{
			PlayerPrefs.SetInt(preferenceKey, GameData.totalScore);
		}

		//set longest Shot Lenth Preference
		int savedLongestShot = PlayerPrefs.GetInt(Common.Key_LogestShot, 0);
		if (GameData.longestShotLength > savedLongestShot)
		{
			PlayerPrefs.SetInt(Common.Key_LogestShot, GameData.longestShotLength);
		}

		int savedComboBest = PlayerPrefs.GetInt(Common.Key_BiggestCombo, 0);
		if (GameData.bestComboValue > savedComboBest)
		{
			PlayerPrefs.SetInt(Common.Key_BiggestCombo, GameData.bestComboValue);
		}

		int ammountToClaim = GameData.totalScore / 100;
		FindFirstObjectByType<TCBRequest>().SetupRequestButton(ammountToClaim);


		//set Homerun Count
		PlayerPrefs.SetInt(Common.Key_TotalHomerunCount, PlayerPrefs.GetInt(Common.Key_TotalHomerunCount, 0) + GameData.homerunCount);
	}

	public void btnBackClicked()
	{
		AppManager.Ins.triggerEvtGamePlayEnd();
		ScenePanelMgr.Ins.showPanel(PanelType.Home_Panel);
		gameObject.SetActive(false);
	}

	public void btnPlayAgainClicked()
	{
		//check if player daily play count exausted
		if (PlayerPlayCountCtrl.Ins.isCanPlay())
		{
			//user can play the game
			PlayerPlayCountCtrl.Ins.incrementPlayCount();
		}
		else
		{
			//Tell youser you cant play the game
			InfoPopup.Ins.showSingleBtnInfoPopup("Daily Play limit reached, please unlock more games", "OK");
			return;
		}

		GameManager.Ins.restartGame();
		gameObject.SetActive(false);
	}


	public void SetupClaimTokens()
	{

		// int ammountToClaim = GameData.totalScore / 100;
		// Debug.Log("total tokens to claim: " + ammountToClaim.ToString());
		// claimText.text = "Click here to claim " + ammountToClaim + " Toxiccandybarium";
		// claimButton.onClick.RemoveAllListeners();
		// claimButton.onClick.AddListener(() => ThirdwebShop.instance.ClaimToken(ammountToClaim.ToString()));

	}
}
