﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class GamePlayUIPanel : MonoBehaviour {

	//Element Holder Game mode Wise
	[Header ("Common in All modes Elements")]
	[SerializeField] GameObject classicModeElementsHolder;
	[SerializeField] GameObject oneOutModeElementsHolder;
	[SerializeField] GameObject speedModeElementsHolder;

	[SerializeField] Text txtBestScore;
	[SerializeField] Text txtCurrentHomerunCount;	//Total homerun count in current running gameplay
	[SerializeField] Text txtCurrentLongestShot;	//Longest Shot that has been scored in current gameplay

	[SerializeField] GameObject ComboPanel;
	[SerializeField] Text txtComboValue;

	[Space (10)]
	[Tooltip ("Show text \"Score\" for classic mode and text \"Homerun\" for OneOut and Speed Mode")]
	[SerializeField] Text lblTopScoreBar;
	[Tooltip ("Show \"Score\" value for classic mode and \"Homerun\" count value for OneOut and Speed Mode")]
	[SerializeField] Text txtTopScoreBarValue;

	//Classic Mode Elements
	[Header ("Classic Mode Elements")]
	[SerializeField] Sprite homeRunSprite;
	[SerializeField] Sprite nonHomerunSprite;
	[SerializeField] Sprite disabledSprite;

	[SerializeField] Image[] ballThrowIndicators;
	[SerializeField] Image[] bonusBallThrowIndicator;

	//OneOut Mode Elements
	[Header ("OneOut Mode Elements")]
	[SerializeField] Sprite oneOutDefaultSprite;
	[SerializeField] Sprite oneOutStrikeSprite;
	[SerializeField] Image[] oneOutStrikeIndicators;

	//Speed Mode Elements
	[Header ("Speed Mode Elements")]
	[SerializeField] Text txtSpeedModeBallLeftToThrow;


	#region Methods Common For All modes
	/// <summary>
	/// Sets the user interface according to mode. This must be called before each game start
	/// </summary>
	public void setUIAccordingToMode(){
		
		if (GameData.gameModeType == GameModeType.Classic) {
			classicModeElementsHolder.SetActive (true);
			oneOutModeElementsHolder.SetActive (false);
			speedModeElementsHolder.SetActive (false);

			lblTopScoreBar.text = "Score";
			txtBestScore.text = PlayerPrefs.GetInt (Common.Key_BestScore_Classic, 0).ToString ();

		} else if (GameData.gameModeType == GameModeType.OneOut) {
			oneOutModeElementsHolder.SetActive (true);
			classicModeElementsHolder.SetActive (false);
			speedModeElementsHolder.SetActive (false);

			lblTopScoreBar.text = "Homerun";
			txtBestScore.text = PlayerPrefs.GetInt (Common.Key_BestScore_OneOut, 0).ToString ();

		} else if (GameData.gameModeType == GameModeType.Speed) {
			speedModeElementsHolder.SetActive (true);
			classicModeElementsHolder.SetActive (false);
			oneOutModeElementsHolder.SetActive (false);

			lblTopScoreBar.text = "Homerun";

			txtSpeedModeBallLeftToThrow.text = GameData.ballLeftToThrow.ToString();
			txtBestScore.text = PlayerPrefs.GetInt (Common.key_BestScore_Speed, 0).ToString ();
		} else {
			Debug.LogError ("Wrong Game mode");
		}

		ComboPanel.SetActive (false);
	}

	/// <summary>
	/// Resets all balls status.
	/// </summary>
	public void resetAllBallsStatus(){

		if (GameData.gameModeType == GameModeType.Classic) {
			for (int i = 0; i < ballThrowIndicators.Length; i++)
				resetBallThrowStatusIndicator (i, false);
			
			for (int i = 0; i < bonusBallThrowIndicator.Length; i++)
				resetBallThrowStatusIndicator (i, true);
		} else if(GameData.gameModeType == GameModeType.OneOut){
			//Reset One out data
			for (int i = 0; i < oneOutStrikeIndicators.Length; i++)
				oneOutStrikeIndicators [i].sprite = oneOutDefaultSprite;

		}else if(GameData.gameModeType == GameModeType.Speed){
			//reset Speed mode data
			txtSpeedModeBallLeftToThrow.text = "0";
		}
	}

	/// <summary>
	/// Sets the total score text.
	/// </summary>
	public void setTotalScoreText(){
		txtTopScoreBarValue.text = GameData.totalScore.ToString ();
	}

	/// <summary>
	/// Sets the homerun count text.
	/// </summary>
	public void setHomerunCountText(){
		txtCurrentHomerunCount.text = GameData.homerunCount.ToString();
	}

	/// <summary>
	/// Sets the longest shot text.
	/// </summary>
	public void setLongestShotText(){
		txtCurrentLongestShot.text = GameData.longestShotLength.ToString();
	}

	/// <summary>
	/// Hides the combo Panel.
	/// </summary>
	public void hideCombo(){
		ComboPanel.SetActive (false);
	}

	/// <summary>
	/// Shows the combo Panel with Updated Combo Value.
	/// </summary>
	public void showCombo(int currentComboValue){
		txtComboValue.text = currentComboValue.ToString ();
		ComboPanel.SetActive (true);
	}

	#endregion Methods Common For All modes

	#region Classic Mode Methods
	/// <summary>
	/// Sets the ball status for passed index thrown ball
	/// </summary>
	/// <param name="ballToThowCount">Ball to thow count.</param>
	/// <param name="ballResult">Ball result.</param>
	public void setBallStatusForClassicMode(int ballToThowCount, BallResultType ballResult, bool isBonus){

		Image[] indicatorsArray = (isBonus == false ? ballThrowIndicators : bonusBallThrowIndicator);

		int indexInArray = (indicatorsArray.Length - 1) - ballToThowCount;
		Image ballIndicator = indicatorsArray[indexInArray];
		
		if (ballResult == BallResultType.HomeRun) {
			ballIndicator.sprite = homeRunSprite;
			ballIndicator.GetComponentInChildren<Text> (true).gameObject.SetActive (true);
		} else {
			ballIndicator.sprite = nonHomerunSprite;
		}
	}

	/// <summary>
	/// Resets the ball throw status indicator.
	/// </summary>
	/// <param name="indexInArray">Index in array.</param>
	/// <param name="isBonus">If set to <c>true</c> is bonus.</param>
	void resetBallThrowStatusIndicator(int indexInArray, bool isBonus){
		Image ballIndicator = (isBonus == false ? ballThrowIndicators[indexInArray] : bonusBallThrowIndicator[indexInArray]);
		ballIndicator.sprite = disabledSprite;
		ballIndicator.GetComponentInChildren<Text> (true).gameObject.SetActive (false);
	}
	#endregion Classic Mode Methods

	#region OneOut Mode Methods
	/// <summary>
	/// Sets the ball status for one out.
	/// </summary>
	/// <param name="ballToThowCount">Ball to thow count.</param>
	/// <param name="ballResult">Ball result.</param>
	public void setBallStatusForOneOut(int ballToThowCount, BallResultType ballResult, bool isBonus){

		if (isBonus) {
			//in case of bonus ball, don't show any indicators
			return;
		}

		if (ballResult == BallResultType.Strike) {
			int indexInArray = (oneOutStrikeIndicators.Length - 1) - ballToThowCount;
			oneOutStrikeIndicators [indexInArray].sprite = oneOutStrikeSprite;
		}
	}
	#endregion OneOut Mode Methods


	#region Speed Mode Methods
	/// <summary>
	/// Sets the ball status for speed.
	/// </summary>
	/// <param name="ballToThowCount">Ball to thow count.</param>
	public void setBallStatusForSpeed(int ballToThowCount){
		txtSpeedModeBallLeftToThrow.text = ballToThowCount.ToString();
	}
	#endregion Speed Mode Methods


	#region Editor mode Work region
	//1. START
//	[SerializeField] GameObject ballThrowIndicatorsHolder;
//	[SerializeField] GameObject bonusBallThrowIndicatorHolder;
//
//	[ContextMenu ("Init Ball Indicator Array For Classic")]
//	void InitBallIndicatorArray_ClassicMode(){
//		//We need to handle the problem that Image 
//		List<Image> tempList = new List<Image>();
//		ballThrowIndicatorsHolder.GetComponentsInChildren<Image> (tempList);
//		tempList.RemoveAt (0);
//		ballThrowIndicators = tempList.ToArray ();
//
//		tempList.Clear ();
//		bonusBallThrowIndicatorHolder.GetComponentsInChildren<Image> (tempList);
//		tempList.RemoveAt (0);
//		bonusBallThrowIndicator = tempList.ToArray ();
//	}
	//1. END
	#endregion Editor mode Work region

}
