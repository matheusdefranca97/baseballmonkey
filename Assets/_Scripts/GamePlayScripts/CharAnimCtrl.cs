﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAnimCtrl : MonoBehaviour {

	Animator animator;
	[SerializeField] float[] menuBlendValueArray;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		startMenuAnim ();
	}

	public void startMenuAnim(){
		transform.localEulerAngles = new Vector3 (0f, 90f, 0f);
		animator.SetBool ("IsMenu", true);
	}

	public void startGamePlayAnim(){
		transform.localEulerAngles = new Vector3 (0f, 0f, 0f); 
		animator.SetBool ("IsMenu", false);
	}

	public void currentMenuAnimClipComplete(){

		float randomValue = menuBlendValueArray[Random.Range(0, menuBlendValueArray.Length)];
		animator.SetFloat ("MenuAnimBlend", randomValue);
	
	}

}
