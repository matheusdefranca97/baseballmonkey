﻿using UnityEngine;
using System;

public class PlayerPlayCountCtrl : MonoBehaviour {

    //free
    public const string Key_Daily_Played_Rounds = "Key_Daily_Played_Rounds";
    public const string Key_Daily_Play_Time = "Key_Daily_Play_Time";

    //paid
    const string Key_Play_One_Day = "Key_Play_One_Day";
    const string Key_Play_One_Week = "Key_Play_One_Week";
    const string Key_Play_One_Month = "Key_Play_One_Month";
    const string Key_Play_One_Year = "Key_Play_One_Year";
    const string Key_Play_Unlimited = "Key_Play_Unlimited";

    const int DailyPlayLimit = 3;

    public static PlayerPlayCountCtrl Ins = null;
    private void Awake() {
        if (Ins != null) {
            Destroy(this);
            return;
        }

        Ins = this;
    }

    public void initializePlayCountPref() {

        PlayerPrefs.SetInt(Key_Daily_Played_Rounds, 0);
        PlayerPrefs.SetString(Key_Daily_Play_Time, "" + DateTime.Now.ToString());
        PlayerPrefs.SetString(Key_Play_One_Day, "");
        PlayerPrefs.SetString(Key_Play_One_Week, "");
        PlayerPrefs.SetString(Key_Play_One_Month, "");
        PlayerPrefs.SetString(Key_Play_One_Year, "");
        PlayerPrefs.SetString(Key_Play_Unlimited, "");
    }

    public void incrementPlayCount() {
        PlayerPrefs.SetInt(Key_Daily_Played_Rounds, PlayerPrefs.GetInt(Key_Daily_Played_Rounds) + 1);
        PlayerPrefs.Save();
    }

    public bool isCanPlay() {

        //check if has purchased any subscription
        //For Unlimited
        if (isAnySubsPurchased()) {
            return true;
        }

        //chk for free rounds
        DateTime lastPlayStart = DateTime.Parse(PlayerPrefs.GetString(Key_Daily_Play_Time));
        if (DateTime.Now >= lastPlayStart.AddDays(1))
            resetDailyRounds();

        int currRounds = PlayerPrefs.GetInt(Key_Daily_Played_Rounds);
        if (currRounds >= DailyPlayLimit)
            return false;

        return true;
    }

    bool isAnySubsPurchased() {

        //check if has purchased any subscription
        //For Unlimited
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString(Key_Play_Unlimited)))
            return true;
        //for yearly
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString(Key_Play_One_Year))) {
            DateTime lastPlayStartYearly = DateTime.Parse(PlayerPrefs.GetString(Key_Play_One_Year));
            if (DateTime.Now < lastPlayStartYearly.AddYears(1))
                return true;
            //reset yearly subscription as it is expired
            PlayerPrefs.SetString(Key_Play_One_Year, "");
            PlayerPrefs.Save();
        }
        //for monthly
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString(Key_Play_One_Month))) {
            DateTime lastPlayStartMonthly = DateTime.Parse(PlayerPrefs.GetString(Key_Play_One_Month));
            if (DateTime.Now < lastPlayStartMonthly.AddMonths(1))
                return true;
            //reset subscription as it is expired
            PlayerPrefs.SetString(Key_Play_One_Month, "");
            PlayerPrefs.Save();
        }
        //for Week
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString(Key_Play_One_Week))) {
            DateTime lastPlayStartWeekly = DateTime.Parse(PlayerPrefs.GetString(Key_Play_One_Week));
            if (DateTime.Now < lastPlayStartWeekly.AddDays(7))
                return true;
            //reset subscription as it is expired
            PlayerPrefs.SetString(Key_Play_One_Week, "");
            PlayerPrefs.Save();
        }
        //for Day
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString(Key_Play_One_Day))) {
            DateTime lastPlayStartDay = DateTime.Parse(PlayerPrefs.GetString(Key_Play_One_Day));
            if (DateTime.Now < lastPlayStartDay.AddDays(1))
                return true;
            //reset subscription as it is expired
            PlayerPrefs.SetString(Key_Play_One_Day, "");
            PlayerPrefs.Save();
        }

        return false;
    }

    void resetDailyRounds() {
        DateTime lastPlayStart = DateTime.Parse(PlayerPrefs.GetString(Key_Daily_Play_Time));
        //		DateTime lastPlayStart1 = new DateTime (DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1, lastPlayStart.Hour, lastPlayStart.Minute, lastPlayStart.Second);
        DateTime newdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 
                                            lastPlayStart.Hour, lastPlayStart.Minute, lastPlayStart.Second);

        print("diff : " + newdate + " " + lastPlayStart);

        PlayerPrefs.SetInt(Key_Daily_Played_Rounds, 0);
        PlayerPrefs.SetString(Key_Daily_Play_Time, "" + newdate.ToString());

    }

    //void giveInAppProduct(string itemid) {
    //    switch (itemid) {
    //        case "banished_monkey_days_1":
    //            PlayerPrefs.SetString(CoinsManager.Key_Play_One_Day, System.DateTime.Now.ToString());
    //            PlayerPrefs.Save();
    //            break;
    //        case "banished_monkey_week_1":
    //            PlayerPrefs.SetString(CoinsManager.Key_Play_One_Week, System.DateTime.Now.ToString());
    //            PlayerPrefs.Save();
    //            break;
    //        case "banished_monkey_month_1":
    //            PlayerPrefs.SetString(CoinsManager.Key_Play_One_Month, System.DateTime.Now.ToString());
    //            PlayerPrefs.Save();
    //            break;
    //        case "banished_monkey_year_1":
    //            PlayerPrefs.SetString(CoinsManager.Key_Play_One_Year, System.DateTime.Now.ToString());
    //            PlayerPrefs.Save();
    //            break;
    //        case "banished_monkey_unlimited":
    //            PlayerPrefs.SetString(CoinsManager.Key_Play_Unlimited, System.DateTime.Now.ToString());
    //            PlayerPrefs.Save();
    //            break;
    //    }
    //}
}
