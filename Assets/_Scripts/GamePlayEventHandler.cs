﻿
//This class handles all the Game Play events is the Game Play
public class GamePlayEventHandler{

	static  GamePlayEventHandler _Ins = null;
	public static GamePlayEventHandler Ins{ 
		get{ 
			if (_Ins == null)
				_Ins = new GamePlayEventHandler ();
			return _Ins;
		}
	} 

	//When game starts
	public delegate void GameStarted();
	public static event GameStarted EvtGameStarted;
	public void triggerEvtGameStarted(){
		EvtGameStarted ();
	}

	//When game starts
	//public delegate void GamePlayEnd();
//	public static event GamePlayEnd EvtGamePlayEnd;
//	public void triggerEvtGamePlayEnd(){
//		EvtGamePlayEnd ();
//	}

	//when ball is thrown by pitcher
	public delegate void BallThrown();
	public static event BallThrown EvtBallThrown;

	//when bat hits the ball
	public delegate void BatHitBall();
	public static event BatHitBall EvtBatHitBall;

	//when single ball throw sequence is complete
	public delegate void SingleBallSeqComplete();
	public static event SingleBallSeqComplete EvtSingleBallSeqComplete;
}
