﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour
{

	GameManager gm;

	public delegate void GameStarted();
	public static event GameStarted EvtGameStart;
	public void triggerEvtGameStarted()
	{
		EvtGameStart();
	}

	public delegate void GamePlayEnd();
	public static event GamePlayEnd EvtGamePlayEnd;
	public void triggerEvtGamePlayEnd()
	{
		EvtGamePlayEnd();
	}

	[SerializeField] GameObject infoPopupGO;

	public static AppManager Ins = null;
	void Awake()
	{
		Ins = this;
		infoPopupGO.SetActive(true);
	}

	// Use this for initialization
	void Start()
	{

		initPreferences();

		gm = GameManager.Ins;
		SceneCanvasMgr.Ins.showCanvas(CanvasType.canvas);
		//StartCoroutine(CO_ShowSplash());
		AudioManager.ins.playMenuBgSound();

	}

	IEnumerator CO_ShowSplash()
	{
		Debug.Log("In Splash.....");
		yield return new WaitForSeconds(3f);
		ScenePanelMgr.Ins.showPanel(PanelType.Splash_Panel);
		//ScenePanelMgr.Ins.hideAllPanels();
		Player plyr = Player.getPlayer();
		if (plyr == null || string.IsNullOrEmpty(plyr.pId))
		{
			Debug.Log("Player NULL");
			//Show Player Login Panel
			//ScenePanelMgr.Ins.showPanel(PanelType.LoginPanel);
		}
		else
		{
			Debug.Log("Player Not NULL");
			ScenePanelMgr.Ins.showPanel(PanelType.Home_Panel);
		}

	}



	void initPreferences()
	{

		if (PlayerPrefs.GetInt(Common.Key_IsPreferenceSaved, 0) == 0)
		{
			PlayerPrefs.SetInt(Common.Key_IsPreferenceSaved, 1);

			//PlayerPrefs.SetInt (Common.Key_SimpleCoinsCount, 0);

			PlayerPrefs.SetInt(Common.Key_Sound, 1);
			PlayerPrefs.SetInt(Common.Key_Music, 1);

			PlayerPrefs.SetString(Common.KEY_PLAYER_DETAILS, "" + JsonUtility.ToJson(new Player()));

			PlayerPlayCountCtrl.Ins.initializePlayCountPref();

			PlayerPrefs.Save();

			//Also show tutorial popup to the user

		}
	}

	#region UI_Button_Call

	public void startGamePlay()
	{

		//check if player daily play count exausted
		/*if (PlayerPlayCountCtrl.Ins.isCanPlay()) {
            //user can play the game
            PlayerPlayCountCtrl.Ins.incrementPlayCount();
        } else {
            //Tell user you cant play the game
            InfoPopup.Ins.showSingleBtnInfoPopup("Daily Play limit reached, please unlock more games", "OK");
            return;
        }*/

		ScenePanelMgr.Ins.showPanel(PanelType.GamePlay_Panel);
		EvtGameStart();
		AudioManager.ins.playGameplayBgSound();
	}


	public void stopGamePlay()
	{
		EvtGamePlayEnd();
	}

	#endregion UI_Button_Call
}
