﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ContestUI : MonoBehaviour {

	private Contest thisContest;
	[SerializeField]
	Image prizeImg;
	[SerializeField]
	Text startDateTxt;
	[SerializeField]
	Text endDateTxt;
	[SerializeField]
	Text descrptionTxt;
	[SerializeField]
	Text contestCoinsTxt;
	Text doNotHaveCoin;


	public GameObject joindButton;
	public GameObject alreadyJoind;
//	bool is_joindContest;

	public void setContest(Contest contest){
		thisContest = contest;
		//set ui for this contest
		startDateTxt.text = contest.start_date;
		endDateTxt.text = contest.end_date;
		descrptionTxt.text = contest.description;
		contestCoinsTxt.text = "Join contest ("+contest.coins;
		if (contest.coins_name == MonkeyCoinType.BountyCoins.ToString ())
			contestCoinsTxt.text += " Bounty Coin)";
		else if (contest.coins_name == MonkeyCoinType.MonkeyCoins.ToString ())
			contestCoinsTxt.text += " Monkey Coin)";
		else if (contest.coins_name == MonkeyCoinType.DablooneyCoins.ToString ())
			contestCoinsTxt.text += " Dabalooney Coin)";
		else if (contest.coins_name == MonkeyCoinType.RaffleCoins.ToString ())
			contestCoinsTxt.text += " Raffle Coin)";
		
		if(!string.IsNullOrEmpty(thisContest.image))
			StartCoroutine (loadImage());



	}

//	void Start()
//	{
//		if(is_joindContest)
//		{
//			joindButton.SetActive(false);
//		}else{
//				joindButton.SetActive(true);
//			}
//	}



	//http://13.59.82.64:8585/user/image/userPhoto-1501768027634.png
	IEnumerator loadImage(){

		WWW download = new WWW (thisContest.image);
		yield return download;

		if (string.IsNullOrEmpty (download.error)) {
			if (download.texture != null) {
				prizeImg.sprite = Sprite.Create (download.texture, new Rect (0, 0, download.texture.width, download.texture.height), Vector2.zero);
			}
		} else {
			print ("img load error"+download.error);
		}

	}


	public void registerForThisContest(){
		print ("register for contest "+thisContest.contest_id+" pay "+thisContest.coins);
		AllCoins myCoins = CoinsManager.ins.getUserCoinDetails ();
		//check if user has sufficient coins to join the contest
		if (thisContest.coins_name == MonkeyCoinType.DablooneyCoins.ToString () && myCoins.DabalooneyCoins >= ulong.Parse(thisContest.coins)) {
			//reduce coins , update to server and register for contest
			myCoins.DabalooneyCoins -= ulong.Parse(thisContest.coins);
			DataInserter.Din.UpdateUserCoins (Player.getPlayer().pId,myCoins,OnUpdateForContest);
			DataInserter.Din.RegisterContest (Player.getPlayer().pId,thisContest.contest_id,OnRegisterForContest);
		} else if (thisContest.coins_name == MonkeyCoinType.RaffleCoins.ToString () && myCoins.RaffleCoins >= ulong.Parse(thisContest.coins)) {
			//reduce coins , update to server and register for contest
			myCoins.RaffleCoins -= ulong.Parse(thisContest.coins);
			DataInserter.Din.UpdateUserCoins (Player.getPlayer().pId,myCoins,OnUpdateForContest);
			DataInserter.Din.RegisterContest (Player.getPlayer().pId,thisContest.contest_id,OnRegisterForContest);			
		}else if (thisContest.coins_name == MonkeyCoinType.BountyCoins.ToString () && myCoins.BountyCoins >= ulong.Parse(thisContest.coins)) {
			//reduce coins , update to server and register for contest
			myCoins.BountyCoins -= ulong.Parse(thisContest.coins);
			DataInserter.Din.UpdateUserCoins (Player.getPlayer().pId,myCoins,OnUpdateForContest);
			DataInserter.Din.RegisterContest (Player.getPlayer().pId,thisContest.contest_id,OnRegisterForContest);
		} else if (thisContest.coins_name == MonkeyCoinType.MonkeyCoins.ToString () && myCoins.MonkeyCoins >= ulong.Parse(thisContest.coins)) {
			//reduce coins , update to server and register for contest
			myCoins.MonkeyCoins -= ulong.Parse(thisContest.coins);
			DataInserter.Din.UpdateUserCoins (Player.getPlayer().pId/* Amit 29/11/17 MyPrefs.ins.getPlayer().pId*/,myCoins,null);
			DataInserter.Din.RegisterContest (Player.getPlayer().pId/* Amit 29/11/17 MyPrefs.ins.getPlayer().pId*/,thisContest.contest_id,OnRegisterForContest);
		} 
		else
		//Amit 29/11/17	PopUpsManager.ins.showSingleButtonPopUp ("You do not have sufficient coins to join this contest");
		InfoPopup.Ins.showSingleBtnInfoPopup("You do not have sufficient coins to join this contest", "OK");
		return;
	}

//	void OnGUI () {
//		GUI.Label (Rect (10, 10, 100, 20), "Hello World!");
//	}

	void OnRegisterForContest(WWW wwwregisterBackup){


		if(!string.IsNullOrEmpty(wwwregisterBackup.error))
		{
			Debug.Log("www OnRegisterForContest error :"+wwwregisterBackup.error);
		}else{
			Debug.Log("www OnRegisterForContest text :"+wwwregisterBackup.text);
			joindButton.SetActive(false);
			alreadyJoind.SetActive(true);
		}

	}

	void OnUpdateForContest(WWW wwwupdateBackup)
	{
		if(!string.IsNullOrEmpty(wwwupdateBackup.error))
		{
			Debug.Log("www wwwupdateBackup error :"+wwwupdateBackup.error);
		}else{
			Debug.Log("www wwwupdateBackup text :"+wwwupdateBackup.text);
			ServerUiManager.ins. getUserScores();
		}
	}

}
