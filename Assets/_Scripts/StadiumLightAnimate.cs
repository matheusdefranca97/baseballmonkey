﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StadiumLightAnimate : MonoBehaviour {

	//Transform[] lightTransArray;

//	// Use this for initialization
//	void Start () {
//		
//	}

	[SerializeField] Material lightMat;
	[SerializeField] float changeSpeed = 0.05f;

	bool isIncreasing = false;
	// Update is called once per frame
	void Update () {

//		for (int i = 0; i < lightTransArray.Length; i++) {
//			lightTransArray [i].Rotate (new Vector3 (0,0,0));
//		}

		if (isIncreasing && lightMat.color.a < 0.95f) {
			lightMat.color += new Color (0, 0, 0, Time.deltaTime*changeSpeed); 
		} else if (!isIncreasing && lightMat.color.a > 0.85f) {
			lightMat.color -= new Color (0, 0, 0, Time.deltaTime*changeSpeed);
		} else {
			isIncreasing = !isIncreasing;
		}
	}
}
