﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameModeSelectPanel : MonoBehaviour {

    [SerializeField]
    Text txtClassicScore;

    [SerializeField]
    Text txtOneOutScore;

    [SerializeField]
    Text txtSpeedScore;

    private void OnEnable() {

        int bestScore = PlayerPrefs.GetInt(Common.Key_BestScore_Classic, 0);
        txtClassicScore.text = "" + bestScore;
        //if (bestScore > 0) {
        //} else {
        //    txtClassicScore.gameObject.SetActive(false);
        //}

        bestScore = PlayerPrefs.GetInt(Common.Key_BestScore_OneOut, 0);
        txtOneOutScore.text = "" + bestScore;
        //if (bestScore > 0) {
        //} else {
        //    txtOneOutScore.gameObject.SetActive(false);
        //}

        bestScore = PlayerPrefs.GetInt(Common.key_BestScore_Speed, 0);
        txtSpeedScore.text = "" + bestScore;
        //if (bestScore > 0) {
        //} else {
        //    txtSpeedScore.gameObject.SetActive(false);
        //}
    }

    //When Game mode classic button is clicked
    public void GameModeClassicSelected(){
		GameData.gameModeType = GameModeType.Classic;
		AppManager.Ins.startGamePlay ();
	}

	//When Game mode OneOut button is clicked
	public void GameModeOneOutSelected(){
		GameData.gameModeType = GameModeType.OneOut;
		AppManager.Ins.startGamePlay ();
	}

	//When game mode Speed button is clicked
	public void GameModeSpeedSelected(){
		GameData.gameModeType = GameModeType.Speed;
		AppManager.Ins.startGamePlay ();
	}

	public void btnBackClicked(){
		ScenePanelMgr.Ins.showPanel (PanelType.Home_Panel);
	}
}
