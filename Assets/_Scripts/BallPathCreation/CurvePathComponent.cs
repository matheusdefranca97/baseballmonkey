﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurvePathComponent : MonoBehaviour {

	[SerializeField] GameObject pathPointPrefab;
	public string pathName = "path";
	[SerializeField] GameObject containerObject;

	[SerializeField] bool isSpecialPath = false;
	public bool IsSpecialPath{
		get{ return isSpecialPath; }
	}

	public CurvePath curvePath;

	public void initAndDrawCurvePath(Vector3 vt0Pos, Vector3 vt1Pos, Vector3 vt2Pos, Vector3 vt3Pos, int calculatePoints, int precision, string pathName, bool drawPath = true){

		this.pathName = pathName;
		curvePath = new CurvePath (vt0Pos, vt1Pos, vt2Pos, vt3Pos, calculatePoints, precision);


		if (drawPath) {
			DrawPathPoints ();
		}
	}

	[ContextMenu ("DrawPathPoints")]
	void DrawPathPoints(){
		containerObject = new GameObject ();
		containerObject.name = pathName;
		containerObject.transform.SetParent (transform);
		containerObject.transform.position = Vector3.zero;

		curvePath.drawPrefabOnPoints (pathPointPrefab, containerObject.transform);
	}

	[ContextMenu ("RemovePath")]
	void RemovePath(){
		//BallPathCreator.pathCompoDict.Remove (pathName);
		Debug.Log ("curve Path removed named: " + pathName);

		DestroyImmediate (this);
	}

	[ContextMenu ("Update Path Points From Prefab")]
	void updatePathPointsFromPrefabs(){
		curvePath.updatePathPointsFromPathPrefabs (containerObject.transform);
	}
}
