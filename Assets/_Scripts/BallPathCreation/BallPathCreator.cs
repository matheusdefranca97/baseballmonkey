﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPathCreator : MonoBehaviour {

	[SerializeField] GameObject pathPointPrefab;
	[SerializeField] Transform PointsContainer; 
	[SerializeField] Transform vt0, vt1, vt2, vt3;
	[SerializeField] int calculatePoints, precision;
	[SerializeField] string pathName = "Path";
//	public CurvePath curvePath;
	[SerializeField] Transform PossibleBezierPointsHolder;

	//public static Dictionary<string, CurvePathComponent> pathCompoDict = new Dictionary<string, CurvePathComponent>();

	// Use this for initialization
	[ContextMenu ("CreateAllCurvedPath")]
	void DrawBezierCurve () {
		int pointsCount = PossibleBezierPointsHolder.childCount;
		for (int i = 0; i < pointsCount; i++) {
			Vector3 pos = PossibleBezierPointsHolder.GetChild (i).position;
			vt1.position = new Vector3 (pos.x, pos.y, vt1.position.z);
			vt2.position = new Vector3 (pos.x, pos.y, vt2.position.z);

			CurvePathComponent pathCompo = gameObject.AddComponent<CurvePathComponent> ();
			pathCompo.initAndDrawCurvePath (vt0.position, vt1.position, vt2.position, vt3.position, calculatePoints, precision, "Path"+ i, true);
		}
	}

	// Use this for initialization
	[ContextMenu ("CreateSingleCurvedPath")]
	void CreateCrossArc () {
		Debug.Log ("Draw Cross Arc");

//		curvePath = new CurvePath (vt0.position, vt1.position, vt2.position, vt3.position, calculatePoints, precision);
//		curvePath.drawPrefabOnPoints (pathPointPrefab, container.transform);

		//		if (pathCompoDict.ContainsKey(pathName)) {
		//			Debug.LogError ("Path With Same Name already exists");
		//			return;
		//		}

		CurvePathComponent pathCompo = gameObject.AddComponent<CurvePathComponent> ();
		pathCompo.initAndDrawCurvePath (vt0.position, vt1.position, vt2.position, vt3.position, calculatePoints, precision, pathName, true);
		//pathCompoDict.Add (pathName, pathCompo);
	}

	//Method to get the random curved path
	List<CurvePathComponent> pathCompoLst = null;
	List<CurvePathComponent> simpleBallCurvePathCompo, specialBallCurvePathCompo;
	public CurvePath getRandomCurvePath(bool isSpecialBallPath){

		if (pathCompoLst == null) {
			pathCompoLst = new List<CurvePathComponent> ();
			gameObject.GetComponents<CurvePathComponent>(pathCompoLst);
			simpleBallCurvePathCompo = pathCompoLst.FindAll (c => c.IsSpecialPath == false);
			specialBallCurvePathCompo = pathCompoLst.FindAll (c => c.IsSpecialPath == true);
		}

		CurvePath curvePath = null;
		if (isSpecialBallPath == false) {
			curvePath = simpleBallCurvePathCompo [Random.Range (0, simpleBallCurvePathCompo.Count)].curvePath;
		} else {
			//if Special ball
			curvePath = specialBallCurvePathCompo [Random.Range (0, specialBallCurvePathCompo.Count)].curvePath;
		}

		return curvePath; //pathCompoArray [Random.Range (0, pathCompoArray.Length)].curvePath;
	}

	////Method to get the path with its name
	//public CurvePath getCurvePathWithName(string pName){

	//	CurvePathComponent namedComponent;
	//	pathCompoDict.TryGetValue(pName, out namedComponent);

	//	if (namedComponent == null) {
	//		return null;
	//	}

	//	return namedComponent.curvePath;
	//}

//	[Header ("throw ball Simulation")]
//	public void throwSimulationBall(){
//		
//	}
}
