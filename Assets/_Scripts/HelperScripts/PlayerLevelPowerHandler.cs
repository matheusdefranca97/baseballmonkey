﻿using UnityEngine;

public static class PlayerLevelPowerHandler {

	const string Key_PlayerLvl = "PlyrLvl";
	const string Key_PlayrScoreForCurrentLvl = "PlyrScoreForCurrentLvl";
	const string Key_ScoreToClearCurrentLvl = "ScoreToClearCurrentLvl";
	const string key_ScoreToClearPreviousLvl = "ScoreToClearPreviousLvl";

	const string Key_playerPower			=	"PlyrPower"; 

	const int scoreToClearLvlOne = 123;

	public static void getLevelInfo(out int level, out int scoreForLvl, out int scoreToClearLvl, out int power){
		level = PlayerPrefs.GetInt (Key_PlayerLvl, 1);
		scoreForLvl = PlayerPrefs.GetInt (Key_PlayrScoreForCurrentLvl, 0);
		scoreToClearLvl = PlayerPrefs.GetInt (Key_ScoreToClearCurrentLvl, scoreToClearLvlOne);
		power = PlayerPrefs.GetInt (Key_playerPower, 100);
	}

	public static void calculateLevel(int homerunCount, int perfect, int great, int nice){

		int level = PlayerPrefs.GetInt (Key_PlayerLvl, 1);
		int scoreForLvl = PlayerPrefs.GetInt (Key_PlayrScoreForCurrentLvl, 0);
		int scoreToClearLvl = PlayerPrefs.GetInt (Key_ScoreToClearCurrentLvl, scoreToClearLvlOne);

		int score = level * (homerunCount * 5 + perfect * 5 + great * 3 + nice);
		score += scoreForLvl;

		if (score >= scoreToClearLvl) {
			int powerValue = PlayerPrefs.GetInt (Key_playerPower, 100);
			PlayerPrefs.GetInt (Key_playerPower, powerValue + 5*level);

			level++;
			PlayerPrefs.SetInt (Key_PlayerLvl, level);

			score = score - scoreToClearLvl;

			int scoreToClearPreviousLvl = PlayerPrefs.GetInt (key_ScoreToClearPreviousLvl, scoreToClearLvlOne);
			PlayerPrefs.SetInt (key_ScoreToClearPreviousLvl, scoreToClearLvl);
			scoreToClearLvl += scoreToClearPreviousLvl;
			PlayerPrefs.SetInt (Key_ScoreToClearCurrentLvl, scoreToClearLvl);

		}

		PlayerPrefs.SetInt (Key_PlayrScoreForCurrentLvl, score);
	}
}
