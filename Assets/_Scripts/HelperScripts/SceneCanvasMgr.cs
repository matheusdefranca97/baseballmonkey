﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//enem for the panel types
public enum CanvasType : byte
{
	None = 0,   //Invalid or Default
	canvas,
	BatSelect_Canvas,
	CharSelect_Canvas,
	BonusCoin_Canvas,
}

public class SceneCanvasMgr : MonoBehaviour
{

	[System.Serializable]

	public class CanvasObject
	{
		public CanvasType type;
		public GameObject canvas;
	}
	public CanvasObject[] canvases;

	//	private Dictionary<byte, GameObject > panels;

	List<CanvasType> canvasTransitions = new List<CanvasType>();

	public static SceneCanvasMgr Ins = null;    //static Instance of the class
	void Awake()
	{

		if (Ins)
		{
			Destroy(Ins);
		}

		Ins = this;
	}

	//	// Use this for initialization
	//	void Start () {
	//
	////		showPanel (PanelType.None);	//this line will hide all  the panels
	//	}

	//	[ContextMenu ("Init Dict")]
	//	void initDictionary(){
	//
	//		panels = new Dictionary<byte, GameObject> ();
	//
	//		for (byte i = 0; i < panelObjects.Length; i++) {
	//			panels.Add ((byte)panelObjects [i].type, panelObjects [i].panel);
	//		}
	//
	//		Debug.LogWarning ("Dictionary initialized without error : " + panels.Count);
	//	}

	//Method to show the panels of passed type
	public void showCanvas(CanvasType typ)
	{

		//Make All the panels active false
		for (byte i = 0; i < canvases.Length; i++)
		{
			if (canvases[i].canvas != null)
			{
				canvases[i].canvas.SetActive(false);
				//Debug.Log("canvases [i] :"+canvases [i]);
			}
		}

		GameObject canvasToActivate = getCanvasFromArray(typ);
		if (canvasToActivate != null)
		{
			//add current panel info to the list
			canvasTransitions.Add(typ);
			canvasToActivate.SetActive(true);
			//			Debug.Log ("Shoe Panel typ : " + typ);
		}
	}

	//Method to show the previous panel
	//return if success
	public bool showPreviousCanvas()
	{

		//first check, if panel transition list count is less than 2 (one for Current, one for previous panel info)
		//i.e (<2) or (<=1)
		if (canvasTransitions.Count < 1)
		{
			return false;
		}

		//Hide current panel
		getCanvasFromArray(canvasTransitions[canvasTransitions.Count - 1]).SetActive(false);

		if (canvasTransitions.Count > 1)
		{
			CanvasType previousTyp = canvasTransitions[canvasTransitions.Count - 2];    //get second last element
																						//		panels [(byte)previousTyp].SetActive (true);	//Wrong Implementation
			getCanvasFromArray(previousTyp).SetActive(true);
		}

		//also remove last element of transition list
		canvasTransitions.RemoveAt(canvasTransitions.Count - 1);

		return true;
	}

	//Method to hide all panels
	public void hideAllCanvas()
	{
		//Make All the panels active false
		for (byte i = 0; i < canvases.Length; i++)
		{
			canvases[i].canvas.SetActive(false);
		}
		canvasTransitions.Clear();
	}

	//Method to get the required panel GameObject from PanelsArray
	private GameObject getCanvasFromArray(CanvasType type)
	{

		if (type == CanvasType.None)
		{
			return null;
		}

		for (byte i = 0; i < canvases.Length; i++)
		{
			if (canvases[i].type == type)
			{
				return canvases[i].canvas;
			}
		}

		return null;
	}
}
