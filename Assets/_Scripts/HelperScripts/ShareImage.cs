﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class ShareImage : MonoBehaviour {
	private bool isProcessing = false;

	private string shareText  = "Hi! checkout this awesome baseball game.\n";
	private string gameLink;
	private string subject = "Best Fun Game";
	private string imageName = "screenshot"; // without the extension, for iinstance, MyPic 

	void Awake()
	{
		//gameLink = "Download the game on play store at " + "\nhttps://play.google.com/store/apps/details?id=" + Application.bundleIdentifier;
	}

	public void shareImage(){

		if(!isProcessing)
			StartCoroutine( ShareScreenshot() );

	}

	private IEnumerator ShareScreenshot(){
		isProcessing = true;
		yield return new WaitForEndOfFrame();

//		Texture2D screenTexture = new Texture2D(1080, 1080,TextureFormat.RGB24,true);
//		screenTexture.Apply();

		var snap = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		snap.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		snap.Apply();
		var screenshot = snap.EncodeToPNG();

		//TextAsset ta = Resources.Load<TextAsset>(imageName);
		//byte[] dataToSave = ta.bytes;

		string destination = Path.Combine(Application.persistentDataPath,System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");
		Debug.Log("destination: " + destination);
//		File.WriteAllText (destination, tex.ToString());


		File.WriteAllBytes(destination, screenshot);

		#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse","file://" + destination);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText + gameLink);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
		intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
		
		currentActivity.Call("startActivity", intentObject);
		
		#endif

		isProcessing = false;

	}

}