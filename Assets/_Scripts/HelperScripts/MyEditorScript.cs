﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyEditorScript : MonoBehaviour {

	[SerializeField] GameObject targetGameObject;

	[ContextMenu ("SetChildrenScaleOne")]
	void setChildrenScaleToOne(){

		if (targetGameObject == null) {
			Debug.Log ("Target Object NULL");
			return;
		}

		RectTransform[] childrenRectTrans = targetGameObject.GetComponentsInChildren<RectTransform> ();

		foreach (RectTransform rectTrans in childrenRectTrans) {
			rectTrans.localScale = Vector3.one;
		}
	}
}
