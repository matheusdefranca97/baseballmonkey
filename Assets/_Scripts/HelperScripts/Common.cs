﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Linq;

//Common class to perform specific tasks through out the game application
public static class Common {

	const int BallToThrowInClassicMode = 20;
	const int BallToThrowInOneOutMode = 3;
	const int BallToThrowInSpeedMode = 5;
	/// <summary>
	/// Method to get the Ball to throw value according to game mode
	/// </summary>
	/// <returns>The ball to throwfor mode.</returns>
	/// <param name="gameModeType">Game mode type.</param>
	public static int getBallToThrowforMode(GameModeType gameModeType){
		int count = 0;
		if (gameModeType == GameModeType.Classic) {
			count = BallToThrowInClassicMode;
		} else if (gameModeType == GameModeType.OneOut) {
			count = BallToThrowInOneOutMode;
		} else if (gameModeType == GameModeType.Speed) {
			count = BallToThrowInSpeedMode;
		}

		return count;
	}

	const int BonusBallToThrowInClassicMode = 5;
	const int BonusBallToThrowInOneOutMode = 1;
	const int BonusBallToThowInSpeedMode = 3;
	public static int getBonusBallToThrowForMode(GameModeType gameModeType){
		int count = 0;
		if (gameModeType == GameModeType.Classic) {
			count = BonusBallToThrowInClassicMode;
		} else if (gameModeType == GameModeType.OneOut) {
			count = BonusBallToThrowInOneOutMode;
		} else if (gameModeType == GameModeType.Speed) {
			count = BonusBallToThowInSpeedMode;
		}

		return count;
	}

	public static int getHomerunCoinsMultiplierForMode(GameModeType gameModeType){
		int value = 1;

		if (gameModeType == GameModeType.Classic) {
			value = 10;
		} else if (gameModeType == GameModeType.OneOut) {
			value = 10;
		} else if (gameModeType == GameModeType.Speed) {
			value = 2;
		}

		return value;
	}

	public static int getPerfectHomerunCoinsMultiplierForMode(GameModeType gameModeType){
		int value = 1;

		if (gameModeType == GameModeType.Classic) {
			value = 50;
		} else if (gameModeType == GameModeType.OneOut) {
			value = 50;
		} else if (gameModeType == GameModeType.Speed) {
			value = 10;
		}

		return value;
	}

	const float DelayTimeBwtBallsThrow_ClassicMode = 3f;// 2f Amit 
	const float DelayTimeBwtBallsThrow_OneOutMode = 2f;
	const float DelayTimeBwtBallThrow_SpeedMode = 0.5f;

	/// <summary>
	/// Gets the delay time bwt balls throw.
	/// </summary>
	/// <returns>The delay time bwt balls throw.</returns>
	/// <param name="gameModeType">Game mode type.</param>
	public static float getDelayTimeBwtBallsThrow(GameModeType gameModeType){
		float delay = 0f;
		if (gameModeType == GameModeType.Classic) {
			delay = DelayTimeBwtBallsThrow_ClassicMode;
		} else if (gameModeType == GameModeType.OneOut) {
			delay = DelayTimeBwtBallsThrow_OneOutMode;
		} else if (gameModeType == GameModeType.Speed) {
			delay = DelayTimeBwtBallThrow_SpeedMode;
		}

		return delay;
	}

	public const string Key_Remove_Add = "Key_Remove_Add";

	//Preferences Keys
	public const string Key_BestScore_Classic	=	"BestScoreClassic";
	public const string Key_BestScore_OneOut	=	"BestScoreOneOut";
	public const string key_BestScore_Speed		=	"BestScoreSpeed";
	public const string Key_LogestShot			=	"LongestShot";
	public const string Key_TotalHomerunCount	=	"TotalHomerunCount";
	public const string Key_BiggestCombo		=	"BiggestCombo";

	//public const string Key_SimpleCoinsCount	=	"SimpleCoins";	
	public const string Key_IsPreferenceSaved	=	"IsPreferenceSaved";

    public const string Key_Sound = "Key_Sound";
    public const string Key_Music = "Key_Music";

    public const string KEY_PLAYER_DETAILS = "Key_Player_Details";
    public const string KEY_IS_TUTORIAL_SHOWN = "is_tutorial_shown";

    public static IEnumerator CO_provideDelay(System.Func<bool> waitUntilCondition, Action delayedAction){

		yield return new WaitUntil (waitUntilCondition);

		delayedAction();
	}

//	public static void ProvideDelay(MonoBehaviour target,float delay, Action delayedAction){
//		target.StartCoroutine (CO_ProvideDelay(delay, delayedAction));
//	}

	public static IEnumerator CO_ProvideDelay(float delay, Action delayedAction){
		if (delay > 0) {
			yield return new WaitForSeconds (delay);
		} else {
			yield return null;
		}

		if (delayedAction != null) {
			delayedAction();
		}
	}

    public static string ConvertToJson<T>(T obj) {
        return JsonUtility.ToJson(obj);
    }

    public static T ConvertFromJson<T>(string jsonData) {
        return JsonUtility.FromJson<T>(jsonData);
    }

    public static Toggle GetActive(this ToggleGroup aGroup) {
        return aGroup.ActiveToggles().FirstOrDefault();
    }

    //UiManager.instance.profilePic.texture = GetCircularTexture (tex2d.height,tex2d.width,tex2d.height/2,tex2d.height/2,tex2d.width/2,tex2d);
    public static Texture2D GetCircularTexture(int h, int w, float r, float cx, float cy, Texture2D sourceTex) {
        //		Color [] c= sourceTex.GetPixels(0, 0, sourceTex.width, sourceTex.height);
        Texture2D b = new Texture2D(h, w);
        for (int i = (int)(cx - r); i < cx + r; i++) {
            for (int j = (int)(cy - r); j < cy + r; j++) {
                float dx = i - cx;
                float dy = j - cy;
                float d = Mathf.Sqrt(dx * dx + dy * dy);
                if (d <= r)
                    b.SetPixel(i - (int)(cx - r), j - (int)(cy - r), sourceTex.GetPixel(i, j));
                else
                    b.SetPixel(i - (int)(cx - r), j - (int)(cy - r), Color.clear);
            }
        }
        b.Apply();
        return b;
    }

    // public const string MatchEmailPattern =
    //	@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
    //            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
    //              + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
    //            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
    //	public static bool IsEmail(string email)
    //	{
    //		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
    //		else return false;
    //	}

    public static bool IsValidEmailAddress(string s) {
        Regex regex = new Regex(@"[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
        return regex.IsMatch(s);
    }
    public static bool IsValidNumber(string s) {
        Regex regex = new Regex(@"(0|[1-9][0-9]*)$");
        return regex.IsMatch(s);
    }

#if UNITY_EDITOR
    //Reset Preference
    [UnityEditor.MenuItem ("Tools/ResetPreferences %#d")]
	public static void resetPreference(){
		PlayerPrefs.DeleteAll ();
		Debug.Log ("All Preference reset");
	}
	#endif
}

public enum BallResultType{
	None,
	Strike,
	Foul,
	HomeRun,
}

public enum BallResultSubType	//Amit 17/5
{
	Nice,Great,Perfact,Simple,None
}
public enum GameModeType{
	None, Classic, OneOut, Speed,
}
