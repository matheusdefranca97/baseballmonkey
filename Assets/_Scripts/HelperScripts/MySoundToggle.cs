﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MySoundToggle : MonoBehaviour {

	[SerializeField] Image	ImgOn;
	[SerializeField] Image	ImgOff;
	
	private bool _isOn; //boolean type field
	//boolean type property
	public bool isOn
	{
		get
		{
			return _isOn;
		}
		set
		{
			_isOn = value;
			isOnFlagUpdated();
		}
	}

	public void toggle_IsOn_Flag(){
        isOn = !isOn;
	}

	private void isOnFlagUpdated(){

		if (_isOn) {
			ImgOn.gameObject.SetActive (true);
			ImgOff.gameObject.SetActive (false);
		} else {
			ImgOff.gameObject.SetActive (true);
			ImgOn.gameObject.SetActive (false);
		}
	}

	void OnEnable(){
		isOnFlagUpdated ();
	}

}
