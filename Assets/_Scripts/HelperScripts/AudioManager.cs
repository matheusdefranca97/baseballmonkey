﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {
	public static AudioManager ins;

	AudioSource source;
	public AudioClip bgSound, menuSound;
	public AudioClip buttonClick;

	public AudioClip baloonHitClip;
	public AudioClip homerunHitClip;
	public AudioClip strike;

	//public AudioClip gotStar;

	void Awake(){
		ins = this;
	}
	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
		playMenuBgSound ();

		////check sound Settings
		//if (PlayerPrefs.GetInt (CoinsManager.Key_Sound) == 0)
		//	UiManager.ins.soundTgl.isOn = false;
		//else
		//	UiManager.ins.soundTgl.isOn = true;
		//if (PlayerPrefs.GetInt (CoinsManager.Key_Music) == 0)
		//	UiManager.ins.musicTgl.isOn = false;
		//else
		//	UiManager.ins.musicTgl.isOn = true;
	}

	public void playMenuBgSound(){
		if (PlayerPrefs.GetInt (Common.Key_Music) != 1) {
            return;
		}

        if (source.clip == menuSound && source.isPlaying) {
            return;
        }

		source.clip = menuSound;
		source.Play ();	
	}

	public void playGameplayBgSound(){
		if (PlayerPrefs.GetInt (Common.Key_Music) != 1) {
            return;
		}

        if (source.clip == bgSound && source.isPlaying) {
            return;
        }

        source.clip = bgSound;
		source.Play ();		
	}

	public void playButtonClickSound(){
		if (PlayerPrefs.GetInt (Common.Key_Sound) == 1) 
			source.PlayOneShot (buttonClick);
	}

	public void playEffectSound(AudioClip audioClip){
		if (PlayerPrefs.GetInt (Common.Key_Sound) == 1) 
			source.PlayOneShot (audioClip);
	}

	public void switchSound(bool isOn){
//		int sound = PlayerPrefs.GetInt (CoinsManager.Key_Sound);
		if (isOn) {
			PlayerPrefs.SetInt (Common.Key_Sound, 1);
		} else {
			PlayerPrefs.SetInt (Common.Key_Sound, 0);
		}
		PlayerPrefs.Save ();		
	}

	public void switchMusic(bool isOn){
		if (isOn) {
			PlayerPrefs.SetInt (Common.Key_Music, 1);
            PlayerPrefs.Save();
            playMenuBgSound ();
		} else {
			PlayerPrefs.SetInt (Common.Key_Music, 0);
	    	PlayerPrefs.Save ();		
			source.Stop ();
		}
	}

}
