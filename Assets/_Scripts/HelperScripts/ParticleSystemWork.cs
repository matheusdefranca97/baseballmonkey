﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemWork : MonoBehaviour {

	[HideInInspector]
	public ParticleSystem _CachedSystem;

	// Use this for initialization
	void Start()
	{
		_CachedSystem = GetComponent<ParticleSystem>();
// _CachedSystem.Play ();

	}

	public void startPArticle()
	{
		_CachedSystem.Play ();
	}
	public void stopParticle()
	{
		_CachedSystem.Stop ();
	}

}
