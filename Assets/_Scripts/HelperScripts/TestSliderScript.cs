﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestSliderScript : MonoBehaviour {

	Slider slider;

	public static TestSliderScript Ins;
	void Awake(){
		Ins = this;
	}

	void Start(){
		slider = GetComponent<Slider> ();
	}

	public void setTimeScele(float timeScale){
		slider.value = timeScale;
	}

	public void OnSliderValueChanged(float value){
		Time.timeScale = value;
	}
}
