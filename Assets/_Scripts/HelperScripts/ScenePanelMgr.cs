﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//enem for the panel types
public enum PanelType : byte
{
	None = 0,   //Invalid or Default
	Home_Panel,
	GamePlay_Panel,
	GameModeSelect_Panel,
	Item_Panel,
	EventShop_Panel,
	Upgrade_Panel,
	Splash_Panel,
	LoginPanel,
	RegisterPanel,
	FbBroker,
	ConvertCoins,
	Contest_Panel
}

public class ScenePanelMgr : MonoBehaviour
{

	[System.Serializable]

	public class PanelObject
	{
		public PanelType type;
		public GameObject panel;
	}
	public PanelObject[] panels;

	//	private Dictionary<byte, GameObject > panels;

	List<PanelType> panelTransitions = new List<PanelType>();

	public static ScenePanelMgr Ins = null; //static Instance of the class
	void Awake()
	{

		if (Ins)
		{
			Destroy(Ins);
		}

		Ins = this;
	}

	// Use this for initialization
	void Start()
	{

		//		showPanel (PanelType.None);	//this line will hide all  the panels
	}

	//	[ContextMenu ("Init Dict")]
	//	void initDictionary(){
	//
	//		panels = new Dictionary<byte, GameObject> ();
	//
	//		for (byte i = 0; i < panelObjects.Length; i++) {
	//			panels.Add ((byte)panelObjects [i].type, panelObjects [i].panel);
	//		}
	//
	//		Debug.LogWarning ("Dictionary initialized without error : " + panels.Count);
	//	}

	//Method to show the panel of passed name
	public void showPanel(string panelTypeName)
	{

		//string[] enumNames = Enum.GetNames(typeof(PanelType);

		//Debug.LogError("show Panel With name");
		var panelType = (PanelType)Enum.Parse(typeof(PanelType), panelTypeName);
		showPanel(panelType);
	}

	//Method to show the panels of passed type
	public void showPanel(PanelType typ)
	{

		//Make All the panels active false
		for (byte i = 0; i < panels.Length; i++)
		{
			if (panels[i].panel != null)
			{
				panels[i].panel.SetActive(false);
			}
		}

		GameObject panelToActivate = getPanelFromArray(typ);
		//Debug.LogError("Panel Type: " + typ.ToString());
		if (panelToActivate != null)
		{
			//add current panel info to the list
			panelTransitions.Add(typ);
			panelToActivate.SetActive(true);
			//			Debug.Log ("Shoe Panel typ : " + typ);
		}
	}

	//Method to show the previous panel
	//return if success
	public void showPreviousPanel()
	{

		//first check, if panel transition list count is less than 2 (one for Current, one for previous panel info)
		//i.e (<2) or (<=1)
		if (panelTransitions.Count < 1)
		{
			return;
		}

		//Hide current panel
		getPanelFromArray(panelTransitions[panelTransitions.Count - 1]).SetActive(false);

		if (panelTransitions.Count > 1)
		{
			PanelType previousTyp = panelTransitions[panelTransitions.Count - 2];   //get second last element
																					//		panels [(byte)previousTyp].SetActive (true);	//Wrong Implementation
			getPanelFromArray(previousTyp).SetActive(true);
		}

		//also remove last element of transition list
		panelTransitions.RemoveAt(panelTransitions.Count - 1);

	}

	//Method to hide all panels
	public void hideAllPanels()
	{
		//Make All the panels active false
		for (byte i = 0; i < panels.Length; i++)
		{
			panels[i].panel.SetActive(false);
		}
		panelTransitions.Clear();
	}

	//Method to get the required panel GameObject from PanelsArray
	private GameObject getPanelFromArray(PanelType type)
	{

		if (type == PanelType.None)
		{
			return null;
		}

		for (byte i = 0; i < panels.Length; i++)
		{
			if (panels[i].type == type)
			{
				return panels[i].panel;
			}
		}

		return null;
	}

	[Space(10)]
	[SerializeField] GameObject internetPanel;
	public void openInternetPanel()
	{
		internetPanel.SetActive(true);
	}

	[SerializeField] GameObject infoPanel;
	public void openWhyInfoPanel()
	{
		infoPanel.SetActive(true);
	}

	[SerializeField] GameObject loadingPanel;
	public void showHideLoading(bool show)
	{
		loadingPanel.SetActive(show);
	}
}
