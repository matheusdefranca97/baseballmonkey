﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalingController : MonoBehaviour {

	[SerializeField] float targetScaleUPValue = 1f;

	Vector3 targetScaleUP;
	Vector3 normalScale;

//	// Use this for initialization
	void Start () {
		normalScale = transform.localScale;
		targetScaleUP = new Vector3 (targetScaleUPValue, targetScaleUPValue, targetScaleUPValue);
	}

	public void ScaleUP(){
		StopAllCoroutines ();

		StartCoroutine (CO_ScaleUP ());
	}

	IEnumerator CO_ScaleUP(){

		yield return null;

		while (transform.localScale.x < targetScaleUP.x) {
			transform.localScale = Vector3.Slerp(transform.localScale, targetScaleUP, Time.deltaTime*10f);
			yield return null;
		}

	}

	public void ScaleDown(){
		StopAllCoroutines ();

		StartCoroutine (CO_ScaleDown ());
	}

	IEnumerator CO_ScaleDown(){

		yield return null;

		while (transform.localScale.x > normalScale.x) {
			transform.localScale = Vector3.Slerp(transform.localScale, normalScale, Time.deltaTime*10f);
			yield return null;
		}
	}
}
