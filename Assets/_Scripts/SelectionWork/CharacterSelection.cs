﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour {

	Animator animator;
	Animation anim;
//	public ScrollRect scroll;
	[HideInInspector]public int Chara_countCollide;
	public static int slectedCharaType = 0;
	public GameObject chracterObj; 


	public GameObject container;

	[SerializeField] Text txtTotalCoinsCount;
	[SerializeField] Text txtCoinsToBuyCurrent;


	bool clickCheck = false;
	private Transform storeLocalScale_2;

	public BoxCollider[] CharaterBoxColliders;


	void Start()
	{
		txtTotalCoinsCount.text = CoinsManager.ins.getUserCoinDetails().MonkeyCoins.ToString();
        //PlayerPrefs.GetInt (Common.Key_SimpleCoinsCount, 0).ToString();
        animator = GetComponent<Animator>();
		CharaterBoxColliders = container.GetComponentsInChildren<BoxCollider> ();
		animator.Play ("Char_Selection_AniClip");
	}

	/*void Update()
	{
		CharacterMenuAni.startMenuAnim ();
	}*/


	void OnTriggerEnter(Collider other)
	{
		for (int i = 0; i < CharaterBoxColliders.Length; i++) {

			//other.transform.localScale = new Vector3 (150, 150, 150);
			if (other.gameObject == CharaterBoxColliders [i].gameObject) {
//				if (clickCheck) {
//					other.transform.localScale = new Vector3 (200, 200, 200);
//					clickCheck = false;
//				} else {
//
//					other.transform.localScale += new Vector3 (50, 50, 50);
//				}

				//TODO
				//Start Scale UP and scale Down coroutine
				other.gameObject.GetComponent<ScalingController>().ScaleUP();

				Chara_countCollide = i+1;
			
				break;
			} 
		}
	}

	void OnTriggerExit(Collider col)
	{	
	
		for (int i = 0; i < CharaterBoxColliders.Length; i++) {
			
			if (col.gameObject == CharaterBoxColliders [i].gameObject) {
//				col.transform.localScale -= new Vector3 (50, 50, 50);
//				//col.transform.localScale = localPos;
//				//stopScroll ();
			
				Chara_countCollide = 0;
				col.gameObject.GetComponent<ScalingController>().ScaleDown();

				break;
			} 
		}

	}

//	IEnumerator CO_ScaleUP(Transform trans){
//
//		yield return null;
//		Vector3 targetScale = new Vector3 (250, 250, 250);
//
//		while (trans.localScale.x < targetScale.x) {
//			trans.localScale = Vector3.Slerp(trans.localScale, targetScale, Time.deltaTime*10f);
//			yield return null;
//		}
//			
//	}
//
//	IEnumerator CO_ScaleDown(Transform trans){
//		
//		yield return null;
//		Vector3 targetScale = new Vector3 (200, 200, 200);
//
//		while (trans.localScale.x > targetScale.x) {
//			trans.localScale = Vector3.Slerp(trans.localScale, targetScale, Time.deltaTime*10f);
//			yield return null;
//		}
//	}

//	void stopScroll()
//	{
//		
//		scroll.StopMovement ();
//		scroll.scrollSensitivity = 0.01f;
//		scroll.movementType = ScrollRect.MovementType.Clamped;
//
//	}

	public void selectChara()
	{
		int coinsCount = int.Parse (txtTotalCoinsCount.text);
		int requiredCoins = int.Parse (txtCoinsToBuyCurrent.text);

		if (coinsCount < requiredCoins) {
			//Not enough coins
			return;
		}

		SceneCanvasMgr.Ins.showCanvas (CanvasType.canvas);
		clickCheck = true;
		slectedCharaType = Chara_countCollide;
		getTexture_Chara ();


	}

	void getTexture_Chara()
	{		
		//Debug.Log ("slectedCharaType  "+slectedCharaType);
		if(slectedCharaType <= 0){
			return;
		}

		Texture newMat = Resources.Load ("body" + (slectedCharaType-1), typeof(Texture)) as Texture;
		(Resources.Load ("player", typeof(Material)) as Material).mainTexture = newMat;
		//chracterObj.GetComponent<Renderer> ().material.mainTexture = newMat;

//		if (slectedCharaType == 1) {			
//			Texture newMat = Resources.Load ("body", typeof(Texture)) as Texture;
//			chracterObj.GetComponent<Renderer> ().material.mainTexture = newMat;
//
//		}
//
//		if (slectedCharaType == 2) {
//			Texture newMat = Resources.Load ("body1", typeof(Texture)) as Texture;
//			chracterObj.GetComponent<Renderer> ().material.mainTexture = newMat;
//		}
//
//		if (slectedCharaType == 3) {
//			Texture newMat = Resources.Load ("body2", typeof(Texture)) as Texture;
//			chracterObj.GetComponent<Renderer> ().material.mainTexture = newMat;
//		}
//
//		if (slectedCharaType == 4) {
//			Texture newMat = Resources.Load ("body3", typeof(Texture)) as Texture;
//			chracterObj.GetComponent<Renderer> ().material.mainTexture = newMat;
//		}
//
//		if (slectedCharaType == 5) {
//			Texture newMat = Resources.Load ("body4", typeof(Texture)) as Texture;
//			chracterObj.GetComponent<Renderer> ().material.mainTexture = newMat;
//		}

	}


 	public	void exitButton()
	{
		clickCheck = true;
		SceneCanvasMgr.Ins.showCanvas (CanvasType.canvas);

	}
}

