﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(ScrollRect))]
public class HorizontalScrollSnap : MonoBehaviour, IDragHandler, IEndDragHandler
{
	// The currently selected inventory "bag"
	public Transform selectedInventoryPanel;
	RectTransform inventoryRectTrans;

	//Top padding and vertical spacing between icons
	public int paddingAndSpacing = 10;
	public float snapSpeed = 10f;

	private int rowCount = 1;
	private List<Vector2> scrollPositions;
	private ScrollRect scrollRect;
	private Vector2 horizontalLerpTarget;
	private bool isLerpingHorizontal;
	private float scrollWidth;
	private RectTransform childRect;

	void Start()
	{
		scrollWidth = (transform as RectTransform).sizeDelta.x;
		scrollRect = gameObject.GetComponent<ScrollRect>();
		scrollRect.inertia = false;
		isLerpingHorizontal = false;


		scrollPositions = new List<Vector2>();
		CalculateChildren();

		childRect = (transform.GetChild(0).transform as RectTransform);

		//(selectedInventoryPanel as RectTransform).position.Set(0, 0, 0);

		inventoryRectTrans = selectedInventoryPanel.GetComponent<RectTransform> ();
		inventoryRectTrans.anchoredPosition = Vector2.zero;
	}

	void CalculateChildren()
	{
		scrollPositions.Clear();
		if (selectedInventoryPanel.childCount > 0)
		{
			int screens = Mathf.CeilToInt((float)selectedInventoryPanel.childCount / (float)rowCount);
			float imgSize = selectedInventoryPanel.GetChild (0).GetComponent<RectTransform> ().sizeDelta.x; //selectedInventoryPanel.GetComponent<GridLayoutGroup>().cellSize.x;

			for (int i = 0; i < screens; ++i)
			{
				int add = paddingAndSpacing;
				if (i == 0)
				{
					add = paddingAndSpacing;
				}
				scrollPositions.Add(new Vector3(i * (imgSize + add), 0f));
//				Debug.Log ((i + 1) + " Screen Position: " + scrollPositions [scrollPositions.Count - 1]);
			}
		}
	}

	void Update()
	{
		if (isLerpingHorizontal)
		{
			inventoryRectTrans.anchoredPosition = Vector2.Lerp(inventoryRectTrans.anchoredPosition, horizontalLerpTarget, snapSpeed * Time.deltaTime);
//			Debug.Log ("Destance: " + Vector3.Distance (selectedInventoryPanel.localPosition, horizontalLerpTarget));
			if (Vector2.Distance(inventoryRectTrans.anchoredPosition, horizontalLerpTarget) < 0.001f)
			{
//				#if UNITY_EDITOR
//				Debug.Log("Local pos: " + inventoryRectTrans.anchoredPosition);
//				#endif

				isLerpingHorizontal = false;
			}
		}
	}

	Vector2 FindClosestFrom(Vector2 start, List<Vector2> positions)
	{
		Vector2 closest = Vector2.zero;
		float distance = Mathf.Infinity;

		foreach (Vector2 position in scrollPositions)
		{
			Vector2 pos =  - new Vector2(position.x, position.y);

//			Debug.Log ("pos: " + pos + ", distance: " + Vector2.Distance(start, pos));
			if (Vector2.Distance(start, pos) < distance)
			{
				distance = Vector2.Distance (start, pos);
//				Debug.Log ("Dis: " + distance);
				closest = pos;
			}
		}

		return closest;
	}

	public void OnDrag(PointerEventData data)
	{
//		Debug.Log ("OnDrag");
		isLerpingHorizontal = false;
	}


	public void OnEndDrag(PointerEventData data)
	{
//		Debug.Log ("OnEndDrag");
		if (scrollRect.horizontal)
		{
//			Debug.Log ("Start Pos: " + inventoryRectTrans.anchoredPosition);
			horizontalLerpTarget = FindClosestFrom(inventoryRectTrans.anchoredPosition, scrollPositions);
			isLerpingHorizontal = true;
		}
	}
}