﻿using Holoville.HOTween;
using UnityEngine;
using UnityEngine.UI;

public class HomePopup : MonoBehaviour
{

    [SerializeField] Text txtBestScore;
    [SerializeField] Text txtLongestShot;
    [SerializeField] Text txtTotalHomerun;
    [SerializeField] Text txtBiggestCombo;

    //[SerializeField] Text txtSimpleCoinsCount;

    [Header("Player Level Display")]
    [SerializeField] Text txtPlayerLvl;
    [SerializeField] Text txtPlayerScoreOutOfTotal;
    [SerializeField] Image imgPlayerLvlProgress;

    [SerializeField] Text txtPower;

    [HideInInspector] public CharAnimCtrl CharacterMenuAni;

    //	public bool is_CharacterChnage = false;

    void Start()
    {
        //CharAnimCtrl scriptToAccess = CharacterMenuAni.GetComponent<CharAnimCtrl>();
        CharacterMenuAni = FindObjectOfType<CharAnimCtrl>();

        soundToggle.isOn = PlayerPrefs.GetInt(Common.Key_Sound, 1) == 1;
        musicToggle.isOn = PlayerPrefs.GetInt(Common.Key_Music, 1) == 1;
        //settingPanel.transform.localScale = new Vector3(1, 0, 1);

        //ShowTutorialPopup();
    }

    void OnEnable()
    {

        txtBestScore.text = PlayerPrefs.GetInt(Common.Key_BestScore_Classic, 0).ToString();
        txtLongestShot.text = PlayerPrefs.GetInt(Common.Key_LogestShot, 0).ToString();
        txtTotalHomerun.text = PlayerPrefs.GetInt(Common.Key_TotalHomerunCount, 0).ToString();
        txtBiggestCombo.text = PlayerPrefs.GetInt(Common.Key_BiggestCombo, 0).ToString();

        //txtSimpleCoinsCount.text = PlayerPrefs.GetInt (Common.Key_SimpleCoinsCount, 0).ToString ();

        //Player Level Value
        int lvl, lvlScore, scoreToClearCurrentLvl, powerValue;
        PlayerLevelPowerHandler.getLevelInfo(out lvl, out lvlScore, out scoreToClearCurrentLvl, out powerValue);

        txtPlayerLvl.text = "" + lvl;
        txtPlayerScoreOutOfTotal.text = lvlScore + "/" + scoreToClearCurrentLvl;
        imgPlayerLvlProgress.fillAmount = lvlScore * 1.0f / scoreToClearCurrentLvl;

        txtPower.text = "" + powerValue;
    }

    void Update()
    {
        if (CharacterMenuAni != null)
            CharacterMenuAni.startMenuAnim();
    }

    public void btnStartGameClicked()
    {
        //adsManager.DisplayInterstitial();
        ScenePanelMgr.Ins.showPanel(PanelType.GameModeSelect_Panel);
        //		is_CharacterChnage = true;
    }

    public void btnItemGameClicked()
    {
        ScenePanelMgr.Ins.showPanel(PanelType.Item_Panel);
    }

    public void btnShopEventGameClicked()
    {
        ScenePanelMgr.Ins.showPanel(PanelType.EventShop_Panel);
    }

    public void btnContest_PanelClicked()
    {
        ScenePanelMgr.Ins.showPanel(PanelType.Contest_Panel);
    }

    public void btnUpgradeGameClicked()
    {
        ScenePanelMgr.Ins.showPanel(PanelType.Upgrade_Panel);
    }

    public void btnBatSelectClicked()
    {
        SceneCanvasMgr.Ins.showCanvas(CanvasType.BatSelect_Canvas);
    }

    public void btnCharSelectClicked()
    {
        SceneCanvasMgr.Ins.showCanvas(CanvasType.CharSelect_Canvas);
    }

    [Header("Setting Panel")]
    [SerializeField] GameObject settingPanel;
    [SerializeField] MySoundToggle soundToggle;
    [SerializeField] MySoundToggle musicToggle;

    public void btnSettingClicked()
    {
        if (settingPanel.transform.localScale.y < 0.1f)
        {
            ShowSettingPanel();
        }
        else if (settingPanel.transform.localScale.y > 0.9f)
        {
            HideSettingPanel();
        }
    }

    void ShowSettingPanel()
    {
        HOTween.To(settingPanel.transform, 0.3f, new TweenParms()
            .Prop("localScale", new Vector3(1, 1, 1), false)
        );
    }

    void HideSettingPanel()
    {
        HOTween.To(settingPanel.transform, 0.3f, new TweenParms()
            .Prop("localScale", new Vector3(1, 0, 1), false)
        );
    }

    public void btnSoundToggleClicked()
    {
        soundToggle.toggle_IsOn_Flag();
        AudioManager.ins.switchSound(soundToggle.isOn);

        //PlayerPrefs.SetInt(Common.Key_Sound, soundToggle.isOn ? 1 : 0);
    }

    public void btnMusicToggleClicked()
    {
        musicToggle.toggle_IsOn_Flag();
        AudioManager.ins.switchMusic(musicToggle.isOn);

        //PlayerPrefs.SetInt(Common.Key_Music, musicToggle.isOn ? 1 : 0);
    }

    // [Header("Tutorial Related")]
    // [SerializeField] GameObject tutorialPopup;
    // [SerializeField] Transform tapHandPointerTrans;
    // void ShowTutorialPopup()
    // {

    //     if (PlayerPrefs.GetInt(Common.KEY_IS_TUTORIAL_SHOWN, -1) != -1)
    //     {
    //         //Tutorial has been shown before
    //         return;
    //     }

    //     //First Time, Show the tutorial
    //     PlayerPrefs.SetInt(Common.KEY_IS_TUTORIAL_SHOWN, 0);

    //     tutorialPopup.SetActive(true);

    //     //HOTween.To(tapHandPointerTrans, 1, new TweenParms()
    //     //    .Prop("localPosition", new Vector3(0, 40, 0), true) // Position tween (set as relative)
    //     //    .Ease(EaseType.EaseInQuad) // Ease
    //     //    .Loops(-1, LoopType.Restart)
    //     //);

    //     Sequence sequence = new Sequence(new SequenceParms().Loops(-1, LoopType.Restart));
    //     // "Append" will add a tween after the previous one/s have completed
    //     sequence.Append(HOTween.To(tapHandPointerTrans, 0.2f, new TweenParms()
    //         .Prop("localPosition", new Vector3(0, 80, 0), true) // Position tween (set as relative)
    //         .Prop("localScale", new Vector3(0.75f, 0.75f, 1), false)
    //         .Ease(EaseType.EaseInQuad) // Ease
    //     ));

    //     sequence.Append(HOTween.To(tapHandPointerTrans, 1f, new TweenParms()
    //         .Prop("localPosition", new Vector3(0, -80, 0), true) // Position tween (set as relative)
    //         .Prop("localScale", new Vector3(1, 1, 1), false)
    //         .Ease(EaseType.EaseInQuad) // Ease
    //     ));

    //     sequence.Play();
    // }

    public void CartURL()
    {
        Application.OpenURL("https://www.zazzle.com/store/3monkeygames");
    }
    public void CartURLTShirt()
    {
        Application.OpenURL("https://www.storefrontier.com/3-monkey-games");
    }
}
