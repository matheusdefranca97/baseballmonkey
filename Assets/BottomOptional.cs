﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomOptional : MonoBehaviour
{

	static bool checkVibrate = false;
	//bool checkSound = false;

	public GameObject vibrateImage;

	//public void Start()
	//{
	//	//vibrateImage.colors.normalColor = new Color(0.22f, 0.22f, 0.22f, 1f);
	//	//vibrateImage.GetComponent<Image>().color = new Color32(255,255,225,100);
	//	//vibrateImage.
	//}

	public void vibrateButtonCheck()
	{
		checkVibrate = !checkVibrate;
		//Debug.Log ("Check bull for vibrate"+checkVibrate);
		if (checkVibrate)
		{
			vibrateImage.GetComponent<Image>().color = new Color32(255, 2, 2, 255);
			//			image.GetComponent<Image>().color = new Color32(255,255,225,100);
		}
		else
		{
			vibrateImage.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
		}
	}

	public static void vibrateMethod()
	{
		if (checkVibrate)
		{
			//Handheld.Vibrate ();
			//Debug.Log ("In Vibrate Method");
		}
	}

	public void backButton()
	{
		Time.timeScale = 0f;
		InfoPopup.Ins.showDoubleBtnInfoPopup("Do You really want to exit this match?", "YES", "NO", () =>
		{
			Time.timeScale = 1f;
			GameManager.Ins.backToHome();
			AppManager.Ins.triggerEvtGamePlayEnd();
			ScenePanelMgr.Ins.showPanel(PanelType.Home_Panel);
		}, () =>
		{
			Time.timeScale = 1f;
		});
	}

	//	public void soundButton()
	//	{
	//		checkSound = !checkSound;
	//	}
}
