﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScript : MonoBehaviour
{

    public string sceneName;
    //public float time;



    // Start is called before the first frame update
    void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        StartCoroutine("goToMenu");
    }

    // Update is called once per frame
    void Update()
    {

    }


    IEnumerator goToMenu()
    {

        yield return new WaitForSeconds(4);
        SceneManager.LoadScene(sceneName);


    }
}
