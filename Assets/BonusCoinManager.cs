using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusCoinManager : MonoBehaviour
{

    [SerializeField] GameObject coinPrefab;

    [SerializeField] Transform startPos;

    int coinPositionHelper;


    private void Awake()
    {
        AppManager.EvtGameStart += GameStarted;
        AppManager.EvtGamePlayEnd += GameEnd;
    }

    void Start()
    {
    }


    public void InstantiateCoin()
    {
        var newCoin = Instantiate(coinPrefab, startPos.position, Quaternion.identity, gameObject.transform);
        newCoin.GetComponent<BonusCoin>().textObject.text = (10000 * (coinPositionHelper + 1)).ToString();

        coinPositionHelper++;
        if (coinPositionHelper >= 4)
        {
            coinPositionHelper = 0;
        }
    }

    public void HitCoin()
    {
        if (GameData.gameModeType == GameModeType.Classic)
            GameData.totalScore += 10000 * (coinPositionHelper + 1);
        InstantiateCoin();
    }


    void GameStarted()
    {
        InstantiateCoin();
        Debug.Log("gameStarted");
    }

    void GameEnd()
    {
        Debug.Log("gameEnd");
        coinPositionHelper = 0;
    }




}
