﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonMove : MonoBehaviour
{
	public Transform startPoint;
	public Transform endPoint;
	public float damping = 6.0f;
	public TextMesh textObject;
	[HideInInspector] public int BonusCount = 0;
	[HideInInspector] public bool isBAlloonBlast;
	public GameObject BonusPanel;

	ParticleSystemWork _particleSystem;
	void Awake()
	{
		_particleSystem = FindObjectOfType<ParticleSystemWork>();
	}


	IEnumerator CO_BonusCount()
	{
		while (BonusCount < 37)
		{
			BonusCount++;
			//textObject.text = "x"+BonusCount.ToString();
			yield return new WaitForSeconds(1f);
			if (BonusCount > 35)
			{
				BonusCount = 0;
			}
		}

	}

	void Start()
	{
		AppManager.EvtGameStart += gameStarted;
		AppManager.EvtGamePlayEnd += gameEnd;

		transform.position = startPoint.position;
	}


	void OnDestroy()
	{
		AppManager.EvtGameStart -= gameStarted;
		AppManager.EvtGamePlayEnd -= gameEnd;
	}

	void gameStarted()
	{
		Debug.Log("gameStarted");
		StartCoroutine("StartBaloonMove");
		StartCoroutine("CO_BonusCount");

	}

	void gameEnd()
	{
		Debug.Log("gameEnd");
		StopCoroutine("StartBaloonMove");
		StopCoroutine("CO_BonusCount");

	}


	IEnumerator StartBaloonMove()
	{
		Vector3 pointA = transform.position;
		while (true)
		{
			//yield return StartCoroutine(MoveObject(transform, startPoint.position, endPoint.position, 150.0f));
			float i = 0.0f;
			float rate = 1.0f / 150.0f;
			while (i < 1.0f)
			{
				i += Time.deltaTime * rate;
				transform.position = Vector3.Lerp(startPoint.position, endPoint.position, i);
				yield return null;
				if (isBAlloonBlast)
				{
					isBAlloonBlast = false;
					break;
				}
			}
			transform.position = startPoint.position;
		}
	}


	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Ball"))
		{
			AudioManager.ins.playEffectSound(AudioManager.ins.baloonHitClip);
			StartCoroutine(Co_WaitForBlast());
		}
	}

	IEnumerator Co_WaitForBlast()
	{
		_particleSystem.startPArticle();
		//GameData.bonusCount = BonusCount;
		//BonusPanel.SetActive(true);
		//BonusPanel.GetComponent<BonusCoinRotate>().wheelStart();

		yield return new WaitForSeconds(0.2f);
		isBAlloonBlast = true;
		transform.position = startPoint.position;

		if (GameData.gameModeType == GameModeType.Classic)
			GameData.totalScore += 10000;

	}

}
