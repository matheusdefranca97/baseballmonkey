﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoPopup : MonoBehaviour
{

	public static InfoPopup Ins;
	void Awake()
	{

		if (InfoPopup.Ins != null)
		{
			Destroy(this.gameObject);
			return;
		}

		InfoPopup.Ins = this;
	}

	// Use this for initialization
	void Start()
	{
		hideInfoPopup();
	}

	public void hideInfoPopup()
	{
		gameObject.SetActive(false);
		singleBtnPanel.SetActive(false);
		doubleBtnPanel.SetActive(false);
	}

	#region Single_btn_panel
	public GameObject singleBtnPanel;
	public Text txtInfoSingleBtnPanel;
	public Text txtSingleBtnDisplay;
	System.Action singleBtnBtnCallBackAction;

	public void showSingleBtnInfoPopup(string msg, string btnDisplayTxt, System.Action singleBtnCallBack = null)
	{
		singleBtnBtnCallBackAction = singleBtnCallBack;
		txtInfoSingleBtnPanel.text = msg;
		txtSingleBtnDisplay.text = btnDisplayTxt;
		gameObject.SetActive(true);
		singleBtnPanel.SetActive(true);
		transform.SetAsLastSibling();
	}

	public void btnClickedOnSingleBtnInfoPopup()
	{
		//play button sound if required
		hideInfoPopup();
		if (singleBtnBtnCallBackAction != null)
		{
			singleBtnBtnCallBackAction.Invoke();
		}
	}

	#endregion //Single_btn_panel

	#region Double_btn_panel

	public GameObject doubleBtnPanel;
	public Text txtInfoDoubleBtnPanel;
	public Text txtLeftBtnDisplay;
	public Text txtRightBtnDisplay;
	System.Action leftBtnCallBackAction;
	System.Action rightBtnCallBackAction;

	public void showDoubleBtnInfoPopup(string msg, string btnLeftDisplay, string btnRightDisplay, System.Action leftBtnCallBack = null, System.Action rightBtnCallBack = null)
	{
		leftBtnCallBackAction = leftBtnCallBack;
		rightBtnCallBackAction = rightBtnCallBack;

		txtInfoDoubleBtnPanel.text = msg;
		txtLeftBtnDisplay.text = btnLeftDisplay;
		txtRightBtnDisplay.text = btnRightDisplay;

		gameObject.SetActive(true);
		doubleBtnPanel.SetActive(true);
		transform.SetAsLastSibling();
	}

	public void btnLeftClicked()
	{
		//Play button sound if required
		hideInfoPopup();

		if (leftBtnCallBackAction != null)
		{
			leftBtnCallBackAction.Invoke();
		}
	}

	public void btnRightClicked()
	{
		//Play button Sound if required
		hideInfoPopup();

		if (rightBtnCallBackAction != null)
		{
			rightBtnCallBackAction.Invoke();
		}
	}

	#endregion //Double_btn_panel
	//	
	//	// Update is called once per frame
	//	void Update () {
	//	
	//	}
}
