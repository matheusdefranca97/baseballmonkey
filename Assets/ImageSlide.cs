using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ImageSlide : MonoBehaviour
{   
    [SerializeField] List<Sprite> allImages;
    [SerializeField] int timeBetweenImages;
    
    // Start is called before the first frame update
    void Start()
    {
        CallImageSlideCoroutine(0);
    }

    void CallImageSlideCoroutine(int id){
        StartCoroutine(ImageSlideCoroutine(id));
    }

    IEnumerator ImageSlideCoroutine(int id){
        Debug.Log("entrei" + id);
        GetComponent<SpriteRenderer>().sprite = allImages[id];
        yield return new WaitForSeconds(timeBetweenImages);
        CallImageSlideCoroutine(GetNextImageID(id));
    }

    public int GetNextImageID(int currentID){
        int nextID;
        Debug.Log(currentID + " currentID");
        if(currentID < allImages.Count - 1){
            nextID = currentID + 1;
        }else{
             nextID = 0;
        }
        return nextID;
    }
}
